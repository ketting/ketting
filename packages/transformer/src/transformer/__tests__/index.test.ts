import { definitionsTransformer, workflowTransformer, bpmnTransformer } from '../index';
import { definitionsFactory } from '../../factory';
import Definitions from '../../model/bpmn/definitions';
import Process from '../../model/bpmn/process';
import ServiceTask from '../../model/bpmn/service-task';
import Workflow from '../../model/workflow/workflow';
import ExclusiveGateway from '../../model/bpmn/exclusive-gateway';
import Step from '../../model/workflow/step';
import Variable from '../../model/workflow/variable';
import Condition from '../../model/workflow/condition';
import Task from '../../model/workflow/task';
import Output from '../../model/workflow/output';

describe('transformer', () => {
  describe('definitions-transformer', () => {
    describe('minimal example', () => {
      it('transforms a minimal workflow to definitions', async () => {
        expect.hasAssertions();
        const workflow = new Workflow('minimal-workflow', 'Minimal Workflow');
        const definitions = await workflowTransformer.toDefinitions(workflow);
        expect(definitions).toBeInstanceOf(Definitions);
        expect(definitions.process).toBeInstanceOf(Process);
        expect(definitions.process.id).toStrictEqual(workflow.id);
        expect(definitions.process.name).toStrictEqual(workflow.name);
      });

      describe('workflow with variables', () => {
        it('transforms a workflow with a step with a variable to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          expect(definitions.process.elements).toHaveLength(3);
          expect(definitions.process.getMessages()).toHaveLength(1);
        });

        it('transforms a workflow with a step with multiple variables to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input'), new Variable('leeftijd', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          expect(definitions.process.elements).toHaveLength(4);
          expect(definitions.process.getMessages()).toHaveLength(2);
        });

        it('transforms a workflow with multiple steps with a variable to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input')]),
            new Step([new Variable('leeftijd', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          expect(definitions.process.elements).toHaveLength(4);
          expect(definitions.process.getMessages()).toHaveLength(2);
        });

        it('transforms a workflow with multiple steps with multiple variables to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input'), new Variable('leeftijd', 'string', 'input')]),
            new Step([new Variable('inkomen', 'string', 'input'), new Variable('vermogen', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          expect(definitions.process.elements).toHaveLength(6);
          expect(definitions.process.getMessages()).toHaveLength(4);
        });
      });

      describe('workflow with conditions', () => {
        it('transforms a workflow with a step with a condition to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([], [], [new Condition('toekenning', 'blue == true', 'right')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(3);
          expect(elements.filter((element) => element instanceof ExclusiveGateway)).toHaveLength(1);
        });

        it('transforms a workflow with a step with multiple conditions to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [],
              [
                new Condition('toekenning', 'blue == true', 'right'),
                new Condition('afwijzing', 'green == true', 'left'),
              ],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(4);
          expect(elements.filter((element) => element instanceof ExclusiveGateway)).toHaveLength(2);
        });

        it('transforms a workflow with multiple steps with a condition to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([], [], [new Condition('toekenning', 'blue == true', 'left')]),
            new Step([], [], [new Condition('afwijzing', 'red == true', 'right')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(4);
          expect(elements.filter((element) => element instanceof ExclusiveGateway)).toHaveLength(2);
        });

        it('transforms a workflow with multiple steps with multiple conditions to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [],
              [
                new Condition('toekenning', 'blue == true', 'right'),
                new Condition('afwijzing', 'green == true', 'left'),
              ],
            ),
            new Step(
              [],
              [],
              [
                new Condition('misschien', 'yellow == true', 'left'),
                new Condition('afwijzing', 'red == true', 'right'),
              ],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(6);
          expect(elements.filter((element) => element instanceof ExclusiveGateway)).toHaveLength(4);
        });
      });

      describe('workflow with tasks', () => {
        it('transforms a workflow with a step with a task to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(3);
          expect(elements.filter((element) => element instanceof ServiceTask)).toHaveLength(1);
        });

        it('transforms a workflow with a step with multiple tasks to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [{ name: 'gemeente', value: '{{gemeente}}' }],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente',
                  }),
                ),
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(4);
          expect(elements.filter((element) => element instanceof ServiceTask)).toHaveLength(2);
        });

        it('transforms a workflow with multiple steps with a task to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [{ name: 'gemeente', value: '{{gemeente}}' }],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente',
                  }),
                ),
              ],
              [],
            ),
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(4);
          expect(elements.filter((element) => element instanceof ServiceTask)).toHaveLength(2);
        });
      });

      describe('workflow with combined elements', () => {
        it('transforms a workflow with a step with a variable followed by a step with a condition to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [new Variable('gemeente', 'string', 'input')],
              [],
              [new Condition('toekenning', 'blue == true', 'right')],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(4);
          expect(elements.filter((element) => element instanceof ExclusiveGateway)).toHaveLength(1);
          expect(definitions.process.getMessages()).toHaveLength(1);
        });

        it('combines steps with variables and/or tasks and/or conditions into definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('complex-workflow', 'Complex Workflow', [
            new Step(
              [new Variable('meerderjarig', 'string', 'input'), new Variable('ontvangt_aow', 'string', 'input')],
              [],
              [
                new Condition('afwijzing', 'meerderjarig == "Nee"', 'left'),
                new Condition('afwijzing', 'ontvangt_aow == "Ja"', 'left'),
              ],
            ),
            new Step(
              [
                new Variable('vermogen_bank', 'string', 'input'),
                new Variable('vermogen_huis', 'string', 'input'),
                new Variable('vermogen_auto', 'string', 'input'),
              ],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({
                    bank: '{{vermogen_bank}}',
                    huis: '{{vermogen_huis}}',
                    auto: '{{vermogen_auto}}',
                  }),
                ),
              ],
              [new Condition('afwijzing', 'vermogen > 32060', 'left')],
            ),
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [
                      { name: 'gemeente', value: '{{gemeente}}' },
                      { name: 'vermogen', value: '{{vermogen}}' },
                    ],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente-vermogen',
                  }),
                ),
              ],
              [new Condition('afwijzing', 'afwijzing == true', 'left')],
            ),
            new Step(
              [],
              [new Task('Verifiable Credential', '{{BASE_URL}}/v1/api/verifiableCredential', 'GET', [])],
              [new Condition('toekenning', 'verifiableCredential', 'right')],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const elements = definitions.process.elements;
          expect(elements).toHaveLength(15);
          expect(elements.filter((element) => element instanceof ServiceTask)).toHaveLength(3);
          expect(elements.filter((element) => element instanceof ExclusiveGateway)).toHaveLength(5);
          expect(definitions.process.getMessages()).toHaveLength(5);
          const flowElements = definitions.process.getFlowElements();
          expect(flowElements).toHaveLength(39);
          expect(flowElements.filter((flowElement) => flowElement['$type'] === 'bpmn:StartEvent')).toHaveLength(1);
          expect(flowElements.filter((flowElement) => flowElement['$type'] === 'bpmn:EndEvent')).toHaveLength(6);
          expect(flowElements.filter((flowElement) => flowElement['$type'] === 'bpmn:ServiceTask')).toHaveLength(3);
          expect(
            flowElements.filter((flowElement) => flowElement['$type'] === 'bpmn:IntermediateCatchEvent'),
          ).toHaveLength(5);
          expect(flowElements.filter((flowElement) => flowElement['$type'] === 'bpmn:ExclusiveGateway')).toHaveLength(
            5,
          );
          expect(flowElements.filter((flowElement) => flowElement['$type'] === 'bpmn:SequenceFlow')).toHaveLength(19);
          const planeElements = definitions.process.getPlaneElements();
          expect(planeElements).toHaveLength(39);
          expect(planeElements.filter((planeElement) => planeElement['$type'] === 'bpmndi:BPMNShape')).toHaveLength(20);
          expect(planeElements.filter((planeElement) => planeElement['$type'] === 'bpmndi:BPMNEdge')).toHaveLength(19);
        });
      });
    });

    describe('workflow-transformer', () => {
      describe('minimal example', () => {
        it('transforms minimal definitions to xml', async () => {
          expect.hasAssertions();
          const definitions = definitionsFactory.create('minimal-workflow', 'Minimal Workflow');
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('process id="minimal-workflow"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="Minimal Workflow"'));
          expect(xml).toStrictEqual(expect.stringContaining('isExecutable="true"'));
        });
      });

      describe('workflow with variables', () => {
        it('transforms a workflow with a step with a variable to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:message'));
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:intermediateCatchEvent'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
        });

        it('transforms a workflow with a step with multiple variables to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input'), new Variable('leeftijd', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___leeftijd"'));
        });

        it('transforms a workflow with multiple steps with a variable to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input')]),
            new Step([new Variable('leeftijd', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___leeftijd"'));
        });

        it('transforms a workflow with multiple steps with multiple variables to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input'), new Variable('leeftijd', 'string', 'input')]),
            new Step([new Variable('inkomen', 'string', 'input'), new Variable('vermogen', 'string', 'input')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___leeftijd"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___inkomen"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___vermogen"'));
        });
      });

      describe('workflow with conditions', () => {
        it('transforms a workflow with a step with a condition to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([], [], [new Condition('toekenning', 'blue == true', 'right')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:exclusiveGateway'));
          expect(xml).toStrictEqual(expect.stringContaining('default'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
        });

        it('transforms a workflow with a step with multiple conditions to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [],
              [
                new Condition('toekenning', 'blue == true', 'right'),
                new Condition('afwijzing', 'green == true', 'left'),
              ],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">green == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="afwijzing"'));
        });

        it('transforms a workflow with multiple steps with a condition to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([], [], [new Condition('toekenning', 'blue == true', 'left')]),
            new Step([], [], [new Condition('afwijzing', 'red == true', 'right')]),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">red == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="afwijzing"'));
        });

        it('transforms a workflow with multiple steps with multiple conditions to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [],
              [
                new Condition('toekenning', 'blue == true', 'right'),
                new Condition('afwijzing', 'green == true', 'left'),
              ],
            ),
            new Step(
              [],
              [],
              [
                new Condition('misschien', 'yellow == true', 'left'),
                new Condition('afwijzing', 'red == true', 'right'),
              ],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">green == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="afwijzing"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">yellow == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="misschien"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">red == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
        });
      });

      describe('workflow with tasks', () => {
        it('transforms a workflow with a step with a task to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:serviceTask'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              'name="Vermogen berekenen"><bpmn2:extensionElements><zeebe:taskDefinition type="http" /><zeebe:taskHeaders><zeebe:header key="url" value="{{BASE_URL}}/v1/api/vermogen/berekenen" /><zeebe:header key="method" value="POST" /><zeebe:header key="body" value="{&#34;vermogensComponenten&#34;:{&#34;huis&#34;:&#34;{{vermogen_huis}}&#34;}}" /></zeebe:taskHeaders></bpmn2:extensionElements>',
            ),
          );
        });

        it('transforms a workflow with a step with multiple tasks to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [{ name: 'gemeente', value: '{{gemeente}}' }],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente',
                  }),
                ),
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('Gemeente DMN'));
          expect(xml).toStrictEqual(expect.stringContaining('Vermogen berekenen'));
        });

        it('transforms a workflow with multiple steps with a task to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [{ name: 'gemeente', value: '{{gemeente}}' }],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente',
                  }),
                ),
              ],
              [],
            ),
            new Step(
              [],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('Gemeente DMN'));
          expect(xml).toStrictEqual(expect.stringContaining('Vermogen berekenen'));
        });
      });

      describe('workflow with combined elements', () => {
        it('transforms a workflow with a step with a variable followed by a step with a condition to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [new Variable('gemeente', 'string', 'input')],
              [],
              [new Condition('toekenning', 'blue == true', 'right')],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('gemeente'));
          expect(xml).toStrictEqual(expect.stringContaining('blue == true'));
        });

        it('combines steps with variables and/or tasks and/or conditions into xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('complex-workflow', 'Complex Workflow', [
            new Step(
              [new Variable('meerderjarig', 'string', 'input'), new Variable('ontvangt_aow', 'string', 'input')],
              [],
              [
                new Condition('afwijzing', 'meerderjarig == "Nee"', 'left'),
                new Condition('afwijzing', 'ontvangt_aow == "Ja"', 'left'),
              ],
            ),
            new Step(
              [
                new Variable('vermogen_bank', 'string', 'input'),
                new Variable('vermogen_huis', 'string', 'input'),
                new Variable('vermogen_auto', 'string', 'input'),
              ],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({
                    bank: '{{vermogen_bank}}',
                    huis: '{{vermogen_huis}}',
                    auto: '{{vermogen_auto}}',
                  }),
                ),
              ],
              [new Condition('afwijzing', 'vermogen > 32060', 'left')],
            ),
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [
                      { name: 'gemeente', value: '{{gemeente}}' },
                      { name: 'vermogen', value: '{{vermogen}}' },
                    ],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente-vermogen',
                  }),
                ),
              ],
              [new Condition('afwijzing', 'afwijzing == true', 'left')],
            ),
            new Step(
              [],
              [new Task('Verifiable Credential', '{{BASE_URL}}/v1/api/verifiableCredential', 'GET', [])],
              [new Condition('toekenning', 'right', 'verifiableCredential')],
            ),
          ]);
          const definitions = await workflowTransformer.toDefinitions(workflow);
          const xml = await definitionsTransformer.toXML(definitions);
          expect(xml).toStrictEqual(expect.stringContaining('api/verifiableCredential'));
          expect(xml).toStrictEqual(
            expect.stringContaining('{&#34;name&#34;:&#34;gemeente&#34;,&#34;value&#34;:&#34;{{gemeente}}&#34;}'),
          );
          expect(xml).toStrictEqual(expect.stringContaining('vermogen &gt; 32060'));
        });
      });
    });

    describe('bpmn-transformer', () => {
      describe('minimal example', () => {
        it('transforms a minimal workflow to definitions', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow');
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('process id="minimal-workflow"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="Minimal Workflow"'));
          expect(xml).toStrictEqual(expect.stringContaining('isExecutable="true"'));
        });
      });

      describe('workflow with variables', () => {
        it('transforms a workflow with a step with a variable to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input')]),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:message'));
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:intermediateCatchEvent'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
        });

        it('transforms a workflow with a step with multiple variables to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input'), new Variable('leeftijd', 'string', 'input')]),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___leeftijd"'));
        });

        it('transforms a workflow with multiple steps with a variable to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input')]),
            new Step([new Variable('leeftijd', 'string', 'input')]),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___leeftijd"'));
        });

        it('transforms a workflow with multiple steps with multiple variables to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([new Variable('gemeente', 'string', 'input'), new Variable('leeftijd', 'string', 'input')]),
            new Step([new Variable('inkomen', 'string', 'input'), new Variable('vermogen', 'string', 'input')]),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___gemeente"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___leeftijd"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___inkomen"'));
          expect(xml).toStrictEqual(expect.stringContaining('name="input_string___vermogen"'));
        });
      });

      describe('workflow with conditions', () => {
        it('transforms a workflow with a step with a condition to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([], [], [new Condition('toekenning', 'blue == true', 'right')]),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:exclusiveGateway'));
          expect(xml).toStrictEqual(expect.stringContaining('default'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
        });

        it('transforms a workflow with a step with multiple conditions to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [],
              [
                new Condition('toekenning', 'blue == true', 'right'),
                new Condition('afwijzing', 'green == true', 'left'),
              ],
            ),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">green == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="afwijzing"'));
        });

        it('transforms a workflow with multiple steps with a condition to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step([], [], [new Condition('toekenning', 'blue == true', 'left')]),
            new Step([], [], [new Condition('afwijzing', 'red == true', 'right')]),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">red == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="afwijzing"'));
        });

        it('transforms a workflow with multiple steps with multiple conditions to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [],
              [
                new Condition('toekenning', 'blue == true', 'right'),
                new Condition('afwijzing', 'green == true', 'left'),
              ],
            ),
            new Step(
              [],
              [],
              [
                new Condition('misschien', 'yellow == true', 'left'),
                new Condition('afwijzing', 'red == true', 'right'),
              ],
            ),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">blue == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="toekenning"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">green == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="afwijzing"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">yellow == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
          expect(xml).toStrictEqual(expect.stringContaining('name="misschien"'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              '<bpmn2:conditionExpression xsi:type="bpmn2:tFormalExpression">red == true</bpmn2:conditionExpression></bpmn2:sequenceFlow>',
            ),
          );
        });
      });

      describe('workflow with tasks', () => {
        it('transforms a workflow with a step with a task to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('bpmn2:serviceTask'));
          expect(xml).toStrictEqual(
            expect.stringContaining(
              'name="Vermogen berekenen"><bpmn2:extensionElements><zeebe:taskDefinition type="http" /><zeebe:taskHeaders><zeebe:header key="url" value="{{BASE_URL}}/v1/api/vermogen/berekenen" /><zeebe:header key="method" value="POST" /><zeebe:header key="body" value="{&#34;vermogensComponenten&#34;:{&#34;huis&#34;:&#34;{{vermogen_huis}}&#34;}}" /></zeebe:taskHeaders></bpmn2:extensionElements>',
            ),
          );
        });

        it('transforms a workflow with a step with multiple tasks to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [{ name: 'gemeente', value: '{{gemeente}}' }],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente',
                  }),
                ),
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('Gemeente DMN'));
          expect(xml).toStrictEqual(expect.stringContaining('Vermogen berekenen'));
        });

        it('transforms a workflow with multiple steps with a task to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [{ name: 'gemeente', value: '{{gemeente}}' }],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente',
                  }),
                ),
              ],
              [],
            ),
            new Step(
              [],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({ vermogensComponenten: { huis: '{{vermogen_huis}}' } }),
                ),
              ],
              [],
            ),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('Gemeente DMN'));
          expect(xml).toStrictEqual(expect.stringContaining('Vermogen berekenen'));
        });
      });

      describe('workflow with combined elements', () => {
        it('transforms a workflow with a step with a variable followed by a step with a condition to xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
            new Step(
              [new Variable('gemeente', 'string', 'input')],
              [],
              [new Condition('toekenning', 'blue == true', 'right')],
            ),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('gemeente'));
          expect(xml).toStrictEqual(expect.stringContaining('blue == true'));
        });

        it('combines steps with variables and/or tasks and/or conditions into xml', async () => {
          expect.hasAssertions();
          const workflow = new Workflow('complex-workflow', 'Complex Workflow', [
            new Step(
              [new Variable('meerderjarig', 'string', 'input'), new Variable('ontvangt_aow', 'string', 'input')],
              [],
              [
                new Condition('afwijzing', 'meerderjarig == "Nee"', 'left'),
                new Condition('afwijzing', 'ontvangt_aow == "Ja"', 'left'),
              ],
            ),
            new Step(
              [
                new Variable('vermogen_bank', 'string', 'input'),
                new Variable('vermogen_huis', 'string', 'input'),
                new Variable('vermogen_auto', 'string', 'input'),
              ],
              [
                new Task(
                  'Vermogen berekenen',
                  '{{BASE_URL}}/v1/api/vermogen/berekenen',
                  'POST',
                  [],
                  JSON.stringify({
                    bank: '{{vermogen_bank}}',
                    huis: '{{vermogen_huis}}',
                    auto: '{{vermogen_auto}}',
                  }),
                ),
              ],
              [new Condition('afwijzing', 'vermogen > 32060', 'left')],
            ),
            new Step(
              [],
              [
                new Task(
                  'Gemeente DMN',
                  '{{BASE_URL}}/v1/api/dmn/evaluate',
                  'POST',
                  [],
                  JSON.stringify({
                    inputs: [
                      { name: 'gemeente', value: '{{gemeente}}' },
                      { name: 'vermogen', value: '{{vermogen}}' },
                    ],
                    dmn: 'https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo',
                    id: 'gemeente-vermogen',
                  }),
                ),
              ],
              [new Condition('afwijzing', 'afwijzing == true', 'left')],
            ),
            new Step(
              [],
              [new Task('Verifiable Credential', '{{BASE_URL}}/v1/api/verifiableCredential', 'GET', [])],
              [new Condition('toekenning', 'verifiableCredential', 'right')],
            ),
          ]);
          const xml = await bpmnTransformer.toBPMN(workflow);
          expect(xml).toStrictEqual(expect.stringContaining('api/verifiableCredential'));
          expect(xml).toStrictEqual(
            expect.stringContaining('{&#34;name&#34;:&#34;gemeente&#34;,&#34;value&#34;:&#34;{{gemeente}}&#34;}'),
          );
          expect(xml).toStrictEqual(expect.stringContaining('vermogen &gt; 32060'));
        });
      });
    });

    describe('workflow with step with task with output mapping', () => {
      it('transforms a workflow with a step with a task with output mapping', async () => {
        expect.hasAssertions();
        const workflow = new Workflow('minimal-workflow', 'Minimal Workflow', [
          new Step(
            [],
            [new Task('Calculate', 'https://www.example.com', 'GET', [new Output('result', 'awesome')])],
            [],
          ),
        ]);
        const xml = await bpmnTransformer.toBPMN(workflow);
        expect(xml).toStrictEqual(
          expect.stringContaining(
            '<zeebe:ioMapping><zeebe:output source="result" target="awesome" /></zeebe:ioMapping>',
          ),
        );
      });
    });
  });
});
