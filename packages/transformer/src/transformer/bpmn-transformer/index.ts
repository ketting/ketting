import WorkflowTransformer from '../workflow-transformer';
import DefinitionsTransformer from '../definitions-transformer';
import Workflow from '../../model/workflow/workflow';

export default class BPMNTransformer {
  constructor(
    readonly workflowTransformer: WorkflowTransformer,
    readonly definitionsTransformer: DefinitionsTransformer,
  ) {}

  async toBPMN(workflow: Workflow): Promise<string> {
    const definitions = await this.workflowTransformer.toDefinitions(workflow);
    return await this.definitionsTransformer.toXML(definitions);
  }
}
