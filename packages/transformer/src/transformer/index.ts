import WorkflowTransformer from './workflow-transformer';
import DefinitionsTransformer from './definitions-transformer';
import BPMNTransformer from './bpmn-transformer';
import { moddle } from '../moddle';
import { definitionsFactory } from '../factory';

export const workflowTransformer = new WorkflowTransformer(definitionsFactory);
export const definitionsTransformer = new DefinitionsTransformer(moddle);
export const bpmnTransformer = new BPMNTransformer(workflowTransformer, definitionsTransformer);
