import Workflow from '../../model/workflow/workflow';
import DefinitionsFactory from '../../factory/bpmn/definitions-factory';

export default class WorkflowTransformer {
  constructor(readonly definitionsFactory: DefinitionsFactory) {}

  async toDefinitions(workflow: Workflow) {
    return this.definitionsFactory.create(workflow.id, workflow.name, workflow.steps);
  }
}
