import Definitions from '../../model/bpmn/definitions';
import { Moddle } from '../../moddle';

export default class DefinitionsTransformer {
  constructor(readonly moddle: Moddle) {}

  static BASE_XML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <bpmn2:definitions
          xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL"
          xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
          xmlns:dc="http://www.omg.org/spec/DD/20100524/DC"
          xmlns:zeebe="http://camunda.org/schema/zeebe/1.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:di="http://www.omg.org/spec/DD/20100524/DI"
          targetNamespace="http://bpmn.io/schema/bpmn"
      >
      </bpmn2:definitions>
  `;

  async toXML(definitionsDto: Definitions): Promise<string> {
    const { rootElement: definitions } = await this.moddle.fromXML(DefinitionsTransformer.BASE_XML);
    definitions.set('id', definitionsDto.id);
    definitions.get('rootElements').push(...definitionsDto.getRootElements());
    definitions.get('diagrams').push(definitionsDto.diagram);
    const definition = await this.moddle.toXML(definitions);
    return definition.xml;
  }
}
