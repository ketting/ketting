import BPMNModdle, { ElementTypes, Definitions, BaseElement } from 'bpmn-moddle';
import { zeebe } from './config/zeebe';

export class Moddle {
  moddle: any;
  constructor() {
    this.moddle = new BPMNModdle({ zeebe });
  }

  create(descriptor: any, attrs?: any): any {
    return this.moddle.create(descriptor, attrs);
  }

  async toXML(definitions: Definitions): Promise<any> {
    return await this.moddle.toXML(definitions);
  }

  async fromXML(xml: string): Promise<any> {
    return await this.moddle.fromXML(xml, () => {
      // work-around to stay consistent with @types/bpmn-moddle
    });
  }
}

export const moddle = new Moddle();
