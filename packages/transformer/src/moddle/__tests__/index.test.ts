import { moddle } from '../index';

const xmlStr = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn2:definitions xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL"
    id="empty-definitions"
    targetNamespace="http://bpmn.io/schema/bpmn">
</bpmn2:definitions>
`;

describe('it instanties bpmn-moddle with zeebe configuration', () => {
  it('can convert from and to xml string according to the example in the readme of the bundle', async () => {
    expect.hasAssertions();
    const { rootElement: definitions } = await moddle.fromXML(xmlStr);
    definitions.set('id', 'NEW ID');
    const bpmnProcess = moddle.create('bpmn:Process', { id: 'MyProcess_1' });
    definitions.get('rootElements').push(bpmnProcess);
    const { xml: xmlStrUpdated } = await moddle.toXML(definitions);
    expect(xmlStrUpdated).toStrictEqual(expect.stringContaining('id="NEW ID"'));
  });

  it('can process a service task of type DMN', async () => {
    expect.hasAssertions();
    const { rootElement: definitions } = await moddle.fromXML(xmlStr);
    const serviceTask = moddle.create('bpmn:ServiceTask', {
      id: 'ServiceTask_1',
      name: 'DMN service worker',
      extensionElements: moddle.create('bpmn:ExtensionElements', {
        values: [
          moddle.create('zeebe:TaskDefinition', { type: 'DMN' }),
          moddle.create('zeebe:TaskHeaders', {
            values: [
              moddle.create('zeebe:Header', {
                key: 'decisionRef',
                value: 'decision-table.dmn',
              }),
            ],
          }),
        ],
      }),
    });
    definitions.get('rootElements').push(serviceTask);
    const { xml: xmlStrUpdated } = await moddle.toXML(definitions);
    expect(xmlStrUpdated).toStrictEqual(expect.stringContaining('decision-table.dmn'));
  });
});
