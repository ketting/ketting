import bpmnTransformer from '../index';
import BPMNTransformer from '../transformer/bpmn-transformer';

describe('exports', () => {
  it('exports a BPMNTransformer instance', () => {
    expect.hasAssertions();
    expect(bpmnTransformer).toBeInstanceOf(BPMNTransformer);
  });
});
