import { Edge } from 'bpmn-moddle';
import { Moddle } from '../../../../moddle';

export default class EdgeFactory {
  constructor(readonly moddle: Moddle) {}

  create(element: any, startX: number, startY: number, endX: number, endY: number, optional: any = {}): Edge {
    return this.moddle.create('bpmndi:BPMNEdge', {
      id: `${element.id}_di`,
      bpmnElement: element,
      waypoint: [
        this.moddle.create('dc:Point', { x: startX, y: startY }),
        this.moddle.create('dc:Point', { x: endX, y: endY }),
      ],
      ...optional,
    });
  }
}
