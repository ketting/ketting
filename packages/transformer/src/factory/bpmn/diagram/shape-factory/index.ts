import { Shape } from 'bpmn-moddle';
import { Moddle } from '../../../../moddle';

export default class ShapeFactory {
  constructor(readonly moddle: Moddle) {
    this.moddle = moddle;
  }

  create(element: any, x: number, y: number, width: number, height: number, optional: any = {}): Shape {
    return this.moddle.create('bpmndi:BPMNShape', {
      bpmnElement: element,
      id: `${element.id}_di`,
      bounds: this.moddle.create('dc:Bounds', { x, y, width, height }),
      ...optional,
    });
  }
}
