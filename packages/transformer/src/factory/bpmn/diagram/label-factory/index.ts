import { Label } from 'bpmn-moddle';
import { Moddle } from '../../../../moddle';

export default class LabelFactory {
  constructor(readonly moddle: Moddle) {}

  create(x: number, y: number, width: number, height: number): Label {
    return this.moddle.create('bpmndi:BPMNLabel', {
      bounds: this.moddle.create('dc:Bounds', { x, y, width, height }),
    });
  }
}
