import DiagramFactory from './diagram-factory';
import ShapeFactory from './shape-factory';
import EdgeFactory from './edge-factory';
import LabelFactory from './label-factory';

import { moddle } from '../../../moddle';
import { prefix } from '../../../config';

export const diagramFactory = new DiagramFactory(moddle, prefix);

export const shapeFactory = new ShapeFactory(moddle);

export const edgeFactory = new EdgeFactory(moddle);

export const labelFactory = new LabelFactory(moddle);
