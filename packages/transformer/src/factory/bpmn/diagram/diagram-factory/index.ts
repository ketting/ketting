import { Diagram } from 'bpmn-moddle';
import { v4 as uuid } from 'uuid';
import Process from '../../../../model/bpmn/process';
import { Moddle } from '../../../../moddle';

export default class DiagramFactory {
  constructor(readonly moddle: Moddle, readonly prefix: string) {}

  create(proc: Process): Diagram {
    return this.moddle.create('bpmndi:BPMNDiagram', {
      id: 'BPMNDiagram_1',
      plane: this.moddle.create('bpmndi:BPMNPlane', {
        id: this.prefix + uuid(),
        bpmnElement: proc.flowElement,
        planeElement: proc.getPlaneElements(),
      }),
    });
  }
}
