import Definitions from '../../../model/bpmn/definitions';
import { v4 as uuid } from 'uuid';
import Step from '../../../model/workflow/step';
import ProcessFactory from '../process-factory';
import DiagramFactory from '../diagram/diagram-factory';

export default class DefinitionsFactory {
  constructor(
    readonly processFactory: ProcessFactory,
    readonly diagramFactory: DiagramFactory,
    readonly prefix: string,
  ) {}

  create(id: string, name: string, steps: Step[] = []) {
    const proc = this.processFactory.create(id, name, steps);
    const diagram = this.diagramFactory.create(proc);
    return new Definitions(this.prefix + uuid(), proc, diagram);
  }
}
