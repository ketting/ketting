import Process from '../../../model/bpmn/process';
import Step from '../../../model/workflow/step';
import EndEventFactory from '../elements/end-event-factory';
import ElementFactory from '../elements/element-factory';
import StartEventFactory from '../elements/start-event-factory';
import { Moddle } from '../../../moddle';

export default class ProcessFactory {
  constructor(
    readonly startEventFactory: StartEventFactory,
    readonly elementFactory: ElementFactory,
    readonly endEventFactory: EndEventFactory,
    readonly moddle: Moddle,
  ) {}

  create(id: string, name: string, steps: Step[]): Process {
    const startEvent = this.startEventFactory.create();
    const elements: any[] = [startEvent];

    steps.forEach((step) => {
      elements.push(...this.elementFactory.createFromVariables(step.variables, elements[elements.length - 1]));
      elements.push(...this.elementFactory.createFromTasks(step.tasks, elements[elements.length - 1]));
      elements.push(...this.elementFactory.createFromConditions(step.conditions, elements[elements.length - 1]));
    });

    const endEvent = this.endEventFactory.create(elements[elements.length - 1]);

    elements.push(endEvent);

    const flowElements = elements.reduce((previous, current) => {
      previous.push(...(current.flowElements || []));
      return previous;
    }, []);

    const flowElement = this.moddle.create('bpmn:Process', {
      id,
      name,
      isExecutable: true,
      flowElements,
    });

    return new Process(id, name, flowElement, elements);
  }
}
