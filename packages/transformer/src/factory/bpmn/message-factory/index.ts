import { Message } from 'bpmn-moddle';
import { v4 as uuid } from 'uuid';
import { Moddle } from '../../../moddle';

export default class MessageFactory {
  constructor(readonly moddle: Moddle, readonly correlationKey: string) {}

  create(name: string): Message {
    return this.moddle.create('bpmn:Message', {
      id: `Message_${uuid()}`,
      name,
      extensionElements: this.moddle.create('bpmn:ExtensionElements', {
        values: [this.moddle.create('zeebe:Subscription', { correlationKey: this.correlationKey })],
      }),
    });
  }
}
