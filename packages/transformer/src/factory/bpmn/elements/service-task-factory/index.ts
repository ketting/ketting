import ServiceTask from '../../../../model/bpmn/service-task';
import ExclusiveGateway from '../../../../model/bpmn/exclusive-gateway';
import { v4 as uuid } from 'uuid';
import Task from '../../../../model/workflow/task';
import ShapeFactory from '../../diagram/shape-factory';
import SequenceFlowFactory from '../sequence-flow-factory';
import EdgeFactory from '../../diagram/edge-factory';
import BaseElement from '../../../../model/bpmn/base-element';
import { Moddle } from '../../../../moddle';

export default class ServiceTaskFactory {
  constructor(
    readonly moddle: Moddle,
    readonly sequenceFlowFactory: SequenceFlowFactory,
    readonly shapeFactory: ShapeFactory,
    readonly edgeFactory: EdgeFactory,
    readonly prefix: string,
  ) {}

  create(task: Task, previousElement: BaseElement): ServiceTask {
    const outgoingSequenceFlowId = this.prefix + uuid();
    const extensionElementValues = [
      this.moddle.create('zeebe:TaskDefinition', { type: 'http' }),
      this.moddle.create('zeebe:TaskHeaders', {
        values: [
          this.moddle.create('zeebe:Header', { key: 'url', value: task.url }),
          this.moddle.create('zeebe:Header', { key: 'method', value: task.method }),
          this.moddle.create('zeebe:Header', { key: 'body', value: task.body }),
        ],
      }),
    ];
    if (task.output.length > 0) {
      extensionElementValues.push(
        this.moddle.create('zeebe:IoMapping', {
          outputParameters: task.output.map((output) =>
            this.moddle.create('zeebe:Output', { source: output.source, target: output.target }),
          ),
        }),
      );
    }
    const serviceTask = this.moddle.create('bpmn:ServiceTask', {
      id: this.prefix + uuid(),
      name: task.name,
      extensionElements: this.moddle.create('bpmn:ExtensionElements', {
        values: extensionElementValues,
      }),
      incoming: [this.sequenceFlowFactory.createDefinition(previousElement.outgoingSequenceFlowId)],
      outgoing: [this.sequenceFlowFactory.createDefinition(outgoingSequenceFlowId)],
    });

    const serviceTaskDiagram = this.shapeFactory.create(serviceTask, 100, previousElement.bottomY + 100, 200, 100);

    const sequenceFlow = this.sequenceFlowFactory.create(
      previousElement.outgoingSequenceFlowId,
      previousElement.sourceElement,
      serviceTask,
    );

    if (previousElement instanceof ExclusiveGateway) {
      previousElement.sourceElement.default = sequenceFlow;
    }

    const sequenceFlowDiagram = this.edgeFactory.create(
      sequenceFlow,
      200,
      previousElement.bottomY,
      200,
      previousElement.bottomY + 100,
    );

    return new ServiceTask(
      [serviceTask, sequenceFlow],
      [serviceTaskDiagram, sequenceFlowDiagram],
      serviceTask,
      outgoingSequenceFlowId,
      previousElement.bottomY + 200,
    );
  }
}
