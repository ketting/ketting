import ExclusiveGateway from '../../../../model/bpmn/exclusive-gateway';
import { v4 as uuid } from 'uuid';
import Condition from '../../../../model/workflow/condition';
import EndEventFactory from '../end-event-factory';
import SequenceFlowFactory from '../sequence-flow-factory';
import ShapeFactory from '../../diagram/shape-factory';
import EdgeFactory from '../../diagram/edge-factory';
import LabelFactory from '../../diagram/label-factory';
import { Moddle } from '../../../../moddle';

export default class ExclusiveGatewayFactory {
  constructor(
    readonly moddle: Moddle,
    readonly endEventFactory: EndEventFactory,
    readonly sequenceFlowFactory: SequenceFlowFactory,
    readonly shapeFactory: ShapeFactory,
    readonly edgeFactory: EdgeFactory,
    readonly labelFactory: LabelFactory,
    readonly prefix: string,
  ) {}

  create(condition: Condition, previousElement: any) {
    const outgoingSequenceFlowId = this.prefix + uuid();
    const sequenceFlowIdEnd = (condition.direction === 'right' ? 'R' : 'L') + uuid();

    const exclusiveGateway = this.moddle.create('bpmn:ExclusiveGateway', {
      id: this.prefix + uuid(),
      incoming: [this.sequenceFlowFactory.createDefinition(previousElement.outgoingSequenceFlowId)],
      outgoing: [
        this.sequenceFlowFactory.createDefinition(sequenceFlowIdEnd),
        this.sequenceFlowFactory.createDefinition(outgoingSequenceFlowId),
      ],
    });

    const exclusiveGatewayDiagram = this.shapeFactory.create(
      exclusiveGateway,
      175,
      previousElement.bottomY + 100,
      50,
      50,
    );

    const endEvent = this.moddle.create('bpmn:EndEvent', {
      id: this.prefix + uuid(),
      name: condition.name,
      incoming: [this.sequenceFlowFactory.createDefinition(sequenceFlowIdEnd)],
    });

    const endEventDiagram = this.shapeFactory.create(
      endEvent,
      condition.direction === 'right' ? 325 : 39,
      previousElement.bottomY + 107,
      36,
      36,
      {
        label: this.labelFactory.create(
          condition.direction === 'right' ? 325 : 39,
          previousElement.bottomY + 86,
          49,
          14,
        ),
      },
    );

    const sequenceFlowEnd = this.sequenceFlowFactory.create(sequenceFlowIdEnd, exclusiveGateway, endEvent, {
      conditionExpression: this.moddle.create('bpmn:FormalExpression', { body: condition.expression }),
    });

    const sequenceFlowEndDiagram = this.edgeFactory.create(
      sequenceFlowEnd,
      condition.direction === 'right' ? 225 : 175,
      previousElement.bottomY + 125,
      condition.direction === 'right' ? 325 : 75,
      previousElement.bottomY + 125,
    );

    const sequenceFlow = this.sequenceFlowFactory.create(
      previousElement.outgoingSequenceFlowId,
      previousElement.sourceElement,
      exclusiveGateway,
    );

    if (previousElement instanceof ExclusiveGateway) {
      previousElement.sourceElement.default = sequenceFlow;
    }

    const sequenceFlowDiagram = this.edgeFactory.create(
      sequenceFlow,
      200,
      previousElement.bottomY,
      200,
      previousElement.bottomY + 100,
    );

    return new ExclusiveGateway(
      [exclusiveGateway, sequenceFlow, sequenceFlowEnd, endEvent],
      [exclusiveGatewayDiagram, sequenceFlowDiagram, sequenceFlowEndDiagram, endEventDiagram],
      exclusiveGateway,
      outgoingSequenceFlowId,
      previousElement.bottomY + 150,
    );
  }
}
