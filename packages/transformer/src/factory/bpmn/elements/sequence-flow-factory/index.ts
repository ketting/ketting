import { FlowElement, SequenceFlow } from 'bpmn-moddle';
import { Moddle } from '../../../../moddle';

export default class SequenceFlowFactory {
  constructor(readonly moddle: Moddle) {}

  createDefinition(id: string): SequenceFlow {
    return this.moddle.create('bpmn:SequenceFlow', { id });
  }

  create(id: string, sourceRef: FlowElement, targetRef: FlowElement, optional: object = {}): SequenceFlow {
    return this.moddle.create('bpmn:SequenceFlow', {
      id,
      sourceRef,
      targetRef,
      ...optional,
    });
  }
}
