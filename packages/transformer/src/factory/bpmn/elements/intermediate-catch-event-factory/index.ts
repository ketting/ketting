import IntermediateCatchEvent from '../../../../model/bpmn/intermediate-catch-event';
import ExclusiveGateway from '../../../../model/bpmn/exclusive-gateway';
import { v4 as uuid } from 'uuid';
import MessageFactory from '../../message-factory';
import SequenceFlowFactory from '../sequence-flow-factory';
import ShapeFactory from '../../diagram/shape-factory';
import EdgeFactory from '../../diagram/edge-factory';
import LabelFactory from '../../diagram/label-factory';
import Variable from '../../../../model/workflow/variable';
import { Moddle } from '../../../../moddle';

export default class IntermediateCatchEventFactory {
  constructor(
    readonly moddle: Moddle,
    readonly messageFactory: MessageFactory,
    readonly sequenceFlowFactory: SequenceFlowFactory,
    readonly shapeFactory: ShapeFactory,
    readonly edgeFactory: EdgeFactory,
    readonly labelFactory: LabelFactory,
    readonly prefix: string,
  ) {}

  create(variable: Variable, previousElement: any): IntermediateCatchEvent {
    const outgoingSequenceFlowId = this.prefix + uuid();
    const namePrefix = variable.source === 'input' ? `input_${variable.type}___` : '';
    const name = `${namePrefix}${variable.name.replace(/-/g, '__').replace(/\./g, '_')}`;
    const message = this.messageFactory.create(name);

    const intermediateCatchEvent = this.moddle.create('bpmn:IntermediateCatchEvent', {
      id: name,
      name,
      eventDefinitions: [
        this.moddle.create('bpmn:MessageEventDefinition', {
          messageRef: message,
        }),
      ],
      incoming: [this.sequenceFlowFactory.createDefinition(previousElement.outgoingSequenceFlowId)],
      outgoing: [this.sequenceFlowFactory.createDefinition(outgoingSequenceFlowId)],
    });

    const intermediateCatchEventDiagram = this.shapeFactory.create(
      intermediateCatchEvent,
      182,
      previousElement.bottomY + 100,
      36,
      36,
      {
        label: this.labelFactory.create(230, previousElement.bottomY + 115, 49, 14),
      },
    );

    const sequenceFlow = this.sequenceFlowFactory.create(
      previousElement.outgoingSequenceFlowId,
      previousElement.sourceElement,
      intermediateCatchEvent,
    );

    if (previousElement instanceof ExclusiveGateway) {
      previousElement.sourceElement.default = sequenceFlow;
    }

    const sequenceFlowDiagram = this.edgeFactory.create(
      sequenceFlow,
      200,
      previousElement.bottomY,
      200,
      previousElement.bottomY + 100,
    );

    return new IntermediateCatchEvent(
      [intermediateCatchEvent, sequenceFlow],
      [intermediateCatchEventDiagram, sequenceFlowDiagram],
      intermediateCatchEvent,
      outgoingSequenceFlowId,
      previousElement.bottomY + 136,
      message,
    );
  }
}
