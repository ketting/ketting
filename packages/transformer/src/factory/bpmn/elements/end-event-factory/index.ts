import EndEvent from '../../../../model/bpmn/end-event';
import ExclusiveGateway from '../../../../model/bpmn/exclusive-gateway';
import { v4 as uuid } from 'uuid';
import SequenceFlowFactory from '../sequence-flow-factory';
import ShapeFactory from '../../diagram/shape-factory';
import EdgeFactory from '../../diagram/edge-factory';
import { Moddle } from '../../../../moddle';

export default class EndEventFactory {
  constructor(
    readonly moddle: Moddle,
    readonly sequenceFlowFactory: SequenceFlowFactory,
    readonly shapeFactory: ShapeFactory,
    readonly edgeFactory: EdgeFactory,
    readonly prefix: string,
  ) {}

  create(previousElement: any): EndEvent {
    const endEvent = this.moddle.create('bpmn:EndEvent', {
      id: this.prefix + uuid(),
      incoming: [this.sequenceFlowFactory.createDefinition(previousElement.outgoingSequenceFlowId)],
    });

    const endEventDiagram = this.shapeFactory.create(endEvent, 182, previousElement.bottomY + 100, 36, 36);

    const sequenceFlow = this.sequenceFlowFactory.create(
      previousElement.outgoingSequenceFlowId,
      previousElement.sourceElement,
      endEvent,
    );

    if (previousElement instanceof ExclusiveGateway) {
      previousElement.sourceElement.default = sequenceFlow;
    }

    const sequenceFlowDiagram = this.edgeFactory.create(
      sequenceFlow,
      200,
      previousElement.bottomY,
      200,
      previousElement.bottomY + 100,
    );

    return new EndEvent([endEvent, sequenceFlow], [endEventDiagram, sequenceFlowDiagram]);
  }
}
