import StartEventFactory from './start-event-factory';
import EndEventFactory from './end-event-factory';
import ElementFactory from './element-factory';
import SequenceFlowFactory from './sequence-flow-factory';
import IntermediateCatchEventFactory from './intermediate-catch-event-factory';
import ExclusiveGatewayFactory from './exclusive-gateway-factory';
import MessageFactory from '../message-factory';
import ServiceTaskFactory from './service-task-factory';
import { moddle } from '../../../moddle';
import { shapeFactory, labelFactory, edgeFactory } from '../diagram';
import { correlationKey, prefix } from '../../../config';

export const sequenceFlowFactory = new SequenceFlowFactory(moddle);

export const startEventFactory = new StartEventFactory(moddle, sequenceFlowFactory, shapeFactory, prefix);

export const endEventFactory = new EndEventFactory(moddle, sequenceFlowFactory, shapeFactory, edgeFactory, prefix);

const messageFactory = new MessageFactory(moddle, correlationKey);

export const intermediateCatchEventFactory = new IntermediateCatchEventFactory(
  moddle,
  messageFactory,
  sequenceFlowFactory,
  shapeFactory,
  edgeFactory,
  labelFactory,
  prefix,
);

export const exclusiveGatewayFactory = new ExclusiveGatewayFactory(
  moddle,
  endEventFactory,
  sequenceFlowFactory,
  shapeFactory,
  edgeFactory,
  labelFactory,
  prefix,
);

export const serviceTaskFactory = new ServiceTaskFactory(
  moddle,
  sequenceFlowFactory,
  shapeFactory,
  edgeFactory,
  prefix,
);

export const elementFactory = new ElementFactory(
  intermediateCatchEventFactory,
  exclusiveGatewayFactory,
  serviceTaskFactory,
);
