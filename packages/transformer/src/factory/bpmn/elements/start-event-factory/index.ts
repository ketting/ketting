import StartEvent from '../../../../model/bpmn/start-event';
import { v4 as uuid } from 'uuid';
import ShapeFactory from '../../diagram/shape-factory';
import SequenceFlowFactory from '../sequence-flow-factory';
import { Moddle } from '../../../../moddle';

export default class StartEventFactory {
  constructor(
    readonly moddle: Moddle,
    readonly sequenceFlowFactory: SequenceFlowFactory,
    readonly shapeFactory: ShapeFactory,
    readonly prefix: string,
  ) {
    this.moddle = moddle;
    this.sequenceFlowFactory = sequenceFlowFactory;
    this.shapeFactory = shapeFactory;
    this.prefix = prefix;
  }

  create(): StartEvent {
    const outgoingSequenceFlowId = this.prefix + uuid();

    const startEvent = this.moddle.create('bpmn:StartEvent', {
      id: this.prefix + uuid(),
      outgoing: [this.sequenceFlowFactory.createDefinition(outgoingSequenceFlowId)],
    });

    const startEventDiagram = this.shapeFactory.create(startEvent, 182, 64, 36, 36);

    return new StartEvent([startEvent], [startEventDiagram], startEvent, outgoingSequenceFlowId, 100);
  }
}
