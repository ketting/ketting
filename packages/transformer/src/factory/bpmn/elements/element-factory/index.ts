import IntermediateCatchEventFactory from '../intermediate-catch-event-factory';
import ExclusiveGatewayFactory from '../exclusive-gateway-factory';
import ServiceTaskFactory from '../service-task-factory';
import BaseElement from '../../../../model/bpmn/base-element';
import IntermediateCatchEvent from '../../../../model/bpmn/intermediate-catch-event';
import ServiceTask from '../../../../model/bpmn/service-task';
import ExclusiveGateway from '../../../../model/bpmn/exclusive-gateway';

export default class ElementFactory {
  constructor(
    readonly intermediateCatchEventFactory: IntermediateCatchEventFactory,
    readonly exclusiveGatewayFactory: ExclusiveGatewayFactory,
    readonly serviceTaskFactory: ServiceTaskFactory,
  ) {}

  createFromVariables(variables: any[], previousElement: any) {
    return this.create(variables, previousElement, this.intermediateCatchEventFactory);
  }

  createFromTasks(tasks: any[], previousElement: any) {
    return this.create(tasks, previousElement, this.serviceTaskFactory);
  }

  createFromConditions(conditions: any[], previousElement: any) {
    return this.create(conditions, previousElement, this.exclusiveGatewayFactory);
  }

  create(
    items: any[] = [],
    previousElement: any,
    factory: ExclusiveGatewayFactory | ServiceTaskFactory | IntermediateCatchEventFactory,
  ): IntermediateCatchEvent[] | ServiceTask[] | ExclusiveGateway[] {
    return items.reduce((previous, current, index) => {
      const lastElement = index > 0 ? previous[previous.length - 1] : previousElement;
      return [...previous, factory.create(current, lastElement)];
    }, []);
  }
}
