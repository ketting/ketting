import DefinitionsFactory from './bpmn/definitions-factory';
import ProcessFactory from './bpmn/process-factory';
import MessageFactory from './bpmn/message-factory';

import { moddle } from '../moddle';
import { correlationKey, prefix } from '../config';
import { startEventFactory, elementFactory, endEventFactory } from './bpmn/elements';
import { diagramFactory } from './bpmn/diagram';

import { v4 as uuid } from 'uuid';

export const messageFactory = new MessageFactory(moddle, correlationKey);

export const processFactory = new ProcessFactory(startEventFactory, elementFactory, endEventFactory, moddle);

export const definitionsFactory = new DefinitionsFactory(processFactory, diagramFactory, prefix);
