import Workflow from '../../model/workflow/workflow';
import StepFactory from './step-factory';
import Step from '../../model/workflow/step';

export default class WorkflowFactory {
  constructor(readonly stepFactory: StepFactory) {}

  create(id: string, name: string, steps: Step[] = []): Workflow {
    return new Workflow(
      id,
      name,
      steps.map((step) => this.stepFactory.create(step.variables, step.tasks, step.conditions)),
    );
  }
}
