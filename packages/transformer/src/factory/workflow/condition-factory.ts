import Condition from '../../model/workflow/condition';

export default class ConditionFactory {
  create(name: string, expression: string, direction: string) {
    return new Condition(name, expression, direction);
  }
}
