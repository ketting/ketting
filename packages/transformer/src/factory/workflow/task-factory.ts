import Task from '../../model/workflow/task';
import Output from '../../model/workflow/output';

export default class TaskFactory {
  create(name: string, url: string, method: string, output: Output[], body?: string): Task {
    return new Task(name, url, method, output, body);
  }
}
