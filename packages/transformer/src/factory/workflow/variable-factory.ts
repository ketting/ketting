import Variable from '../../model/workflow/variable';

export default class VariableFactory {
  create(name: string, type: string, source: string): Variable {
    return new Variable(name, type, source);
  }
}
