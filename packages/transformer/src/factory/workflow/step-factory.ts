import Step from '../../model/workflow/step';
import VariableFactory from './variable-factory';
import TaskFactory from './task-factory';
import ConditionFactory from './condition-factory';
import Variable from '../../model/workflow/variable';
import Task from '../../model/workflow/task';
import Condition from '../../model/workflow/condition';

export default class StepFactory {
  constructor(
    readonly variableFactory: VariableFactory,
    readonly taskFactory: TaskFactory,
    readonly conditionFactory: ConditionFactory,
  ) {}

  create(variables: Variable[] = [], tasks: Task[] = [], conditions: Condition[] = []) {
    return new Step(
      variables.map((variable) => this.variableFactory.create(variable.name, variable.type, variable.source)),
      tasks.map((task) => this.taskFactory.create(task.name, task.url, task.method, task.output, task.body)),
      conditions.map((condition) =>
        this.conditionFactory.create(condition.name, condition.expression, condition.direction),
      ),
    );
  }
}
