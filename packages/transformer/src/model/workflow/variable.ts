export default class Variable {
  name: string;
  type: string;
  source: string;

  constructor(name: string, type: string, source: string) {
    this.name = name;
    this.type = type;
    this.source = source;
  }
}
