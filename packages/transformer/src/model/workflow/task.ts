import Output from './output';

export default class Task {
  name: string;

  url: string;

  method: string;

  output: Output[];

  body?: string;

  constructor(name: string, url: string, method: string, output: Output[], body?: string) {
    this.name = name;
    this.method = method;
    this.url = url;
    this.output = output;
    this.body = body;
  }
}
