import Step from './step';

export default class Workflow {
  id: string;

  name: string;

  steps: Step[];

  constructor(id: string, name: string, steps: Step[] = []) {
    this.id = id;
    this.name = name;
    this.steps = steps;
  }
}
