export default class Condition {
  name: string;

  expression: string;

  direction: string;

  constructor(name: string, expression: string, direction: string = 'right') {
    this.name = name;
    this.expression = expression;
    this.direction = direction;
  }
}
