import Variable from './variable';
import Task from './task';
import Condition from './condition';

export default class Step {
  variables: Variable[];

  tasks: Task[];

  conditions: Condition[];

  constructor(variables: Variable[] = [], tasks: Task[] = [], conditions: Condition[] = []) {
    this.variables = variables;
    this.tasks = tasks;
    this.conditions = conditions;
  }
}
