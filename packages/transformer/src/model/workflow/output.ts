export default class Output {
  source: string;
  target: string;
  constructor(source: string, target: string) {
    this.source = source;
    this.target = target;
  }
}
