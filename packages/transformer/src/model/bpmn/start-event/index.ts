import { FlowElement, DiagramElement } from 'bpmn-moddle';
import BaseElement from '../base-element';

export default class StartEvent implements BaseElement {
  flowElements: FlowElement[];
  diagramElements: DiagramElement[];
  sourceElement: FlowElement;
  outgoingSequenceFlowId: string;
  bottomY: number;
  constructor(
    flowElements: FlowElement[],
    diagramElements: DiagramElement[],
    sourceElement: FlowElement,
    outgoingSequenceFlowId: string,
    bottomY: number,
  ) {
    this.flowElements = flowElements;
    this.diagramElements = diagramElements;
    this.sourceElement = sourceElement;
    this.outgoingSequenceFlowId = outgoingSequenceFlowId;
    this.bottomY = bottomY;
  }
}
