import { FlowElement, DiagramElement, Message } from 'bpmn-moddle';
import BaseElement from '../base-element';

export default class IntermediateCatchEvent implements BaseElement {
  flowElements: FlowElement[];
  diagramElements: DiagramElement[];
  sourceElement: FlowElement;
  outgoingSequenceFlowId: string;
  bottomY: number;
  message: any;
  constructor(
    flowElements: FlowElement[],
    diagramElements: DiagramElement[],
    sourceElement: FlowElement,
    outgoingSequenceFlowId: string,
    bottomY: number,
    message: Message,
  ) {
    this.flowElements = flowElements;
    this.diagramElements = diagramElements;
    this.sourceElement = sourceElement;
    this.outgoingSequenceFlowId = outgoingSequenceFlowId;
    this.bottomY = bottomY;
    this.message = message;
  }
}
