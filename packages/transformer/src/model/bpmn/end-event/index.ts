import { FlowElement, DiagramElement } from 'bpmn-moddle';

export default class EndEvent {
  flowElements: FlowElement[];
  diagramElements: DiagramElement[];
  constructor(flowElements: FlowElement[], diagramElements: DiagramElement[]) {
    this.flowElements = flowElements;
    this.diagramElements = diagramElements;
  }
}
