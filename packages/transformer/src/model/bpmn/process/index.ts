import IntermediateCatchEvent from '../intermediate-catch-event';
import { FlowElementsContainer, DiagramElement, FlowElement, Message } from 'bpmn-moddle';

export default class Process {
  id: string;

  name: string;

  flowElement: FlowElementsContainer;

  elements: any[];

  constructor(id: string, name: string, flowElement: FlowElementsContainer, elements: any[] = []) {
    this.id = id;
    this.name = name;
    this.flowElement = flowElement;
    this.elements = elements;
  }

  getMessages(): Message[] {
    return this.elements
      .filter((element) => element instanceof IntermediateCatchEvent)
      .map((element) => element.message);
  }

  getFlowElements(): FlowElement[] {
    return this.flowElement.flowElements;
  }

  getPlaneElements(): DiagramElement[] {
    return this.elements.reduce((previous, current) => {
      previous.push(...current.diagramElements);
      return previous;
    }, []);
  }
}
