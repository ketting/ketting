import { FlowElement, DiagramElement } from 'bpmn-moddle';

export default interface BaseElement {
  flowElements: FlowElement[];
  diagramElements: DiagramElement[];
  sourceElement: FlowElement;
  outgoingSequenceFlowId: string;
  bottomY: number;
}
