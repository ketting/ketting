import { ExclusiveGateway as ModdleExclusiveGateway, DiagramElement, FlowElement } from 'bpmn-moddle';
import BaseElement from '../base-element';

export default class ExclusiveGateway implements BaseElement {
  flowElements: FlowElement[];
  diagramElements: DiagramElement[];
  sourceElement: ModdleExclusiveGateway;
  outgoingSequenceFlowId: string;
  bottomY: number;
  constructor(
    flowElements: FlowElement[],
    diagramElements: DiagramElement[],
    sourceElement: ModdleExclusiveGateway,
    outgoingSequenceFlowId: string,
    bottomY: number,
  ) {
    this.flowElements = flowElements;
    this.diagramElements = diagramElements;
    this.sourceElement = sourceElement;
    this.outgoingSequenceFlowId = outgoingSequenceFlowId;
    this.bottomY = bottomY;
  }
}
