import Process from '../process';

export default class Definitions {
  id: string;

  process: Process;

  diagram: any;

  constructor(id: string, proc: Process, diagram: any) {
    this.id = id;
    this.process = proc;
    this.diagram = diagram;
  }

  getRootElements() {
    return [this.process.flowElement, ...this.process.getMessages()];
  }
}
