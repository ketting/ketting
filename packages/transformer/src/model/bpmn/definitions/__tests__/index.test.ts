import Definitions from '../index';
import Process from '../../process';
import { moddle } from '../../../../moddle';
import IntermediateCatchEvent from '../../intermediate-catch-event';

describe('definitions', () => {
  it('gets rootElements', () => {
    expect.hasAssertions();
    const message = moddle.create('bpmn:Message', { id: 'my-message' });
    const intermediateCatchEvent = new IntermediateCatchEvent(
      [],
      [],
      moddle.create('bpmn:IntermediateCatchEvent'),
      'outgoing-id',
      32,
      message,
    );
    const p = new Process('p', 'p', moddle.create('bpmn:Process'), [intermediateCatchEvent]);
    const definitions = new Definitions('5', p, {});
    const rootElements = definitions.getRootElements();
    expect(rootElements).toStrictEqual([p.flowElement, message]);
  });
});
