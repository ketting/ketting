# BPMN Transformer

Transforms javascript objects to BPMN strings.

## Install

### npm

```
npm install @ovrhd/bpmn-transformer
```

### yarn

```
yarn add @ovrhd/bpmn-transformer
```

## Example

```
import bpmnTransformer from '@ovrhd/bpmn-transformer';

const data = {
    id: 'minimal-workflow',
    name: 'Minimal Workflow',
    steps: [
        {
            variables: [
                { name: 'meerderjarig' },
                { name: 'gemeente' }
            ],
            tasks: [
                {
                    name: 'DMN workflow gemeente',
                    url: 'https://{{BASE_URL}}/v1/api/decisions/evaluate',
                    method: 'POST',
                    body: JSON.stringify({
                        id: "meerderjarig-gemeente.dmn",
                        dmn: "https://ipfs.io/ipfs/QmSwA9mRJeLVSo7F2Pjj5rKeAgirphN43TKn1gwwGmgiPo",
                        inputs: {
                            gemeente: "{{gemeente}}",
                            meerderjarig: "{{meerderjarig}}",
                        },
                    }),
                },
            ],
            conditions: [
                {
                    name: 'Rejection',
                    direction: 'left',
                    expression: 'rejection == true'
                }
            ]
        },
    ],
}

const xml = bpmnTransformer.toBPMN(data)
```
