module.exports = {
  someSidebar: {
    Ketting: [
      'ketting/introduction',
      {
        type: 'category',
        label: 'Architecture',
        collapsed: false,
        items: [
          'ketting/architecture/system',
          'ketting/architecture/container',
          'ketting/architecture/component',
          'ketting/architecture/dynamic',
          'ketting/architecture/deployment',
        ],
      },
      {
        type: 'category',
        label: 'Infrastructure',
        collapsed: false,
        items: [
          'ketting/infrastructure/development',
          'ketting/infrastructure/devops',
        ],
      },
      'ketting/zoekhulp',
    ],
  },
};
