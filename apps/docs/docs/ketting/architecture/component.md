---
id: component
title: Component Architecture
sidebar_label: Components
description: Ketting Component Architecture
slug: '/architecture/components'
---

The following components of Ketting can be distinguished:
 - Process execution
 - Process administration

## Process execution

Process definitions are loaded into the front-end web application for anonymous users through an API call to the 
front-end webserver. When a pallet variable is presented, the webserver will request a session to the Wallet SSI and 
saves the session data in redis for 1 minute. The webserver then listens to credential data shared by user in Wallet SSI 
via webhook and verifies that credential in 2 ways, first by the credential identifier in Redis in case a credential has 
been saved before and then by making verification API call to Service Ledger. Afterwards, the webserver proxies the 
request to a secured internal connection to the Ketting core API. The process definitions are visually presented to help 
the user select a Process to execute. The selected Process is started via the same API route. Actual execution of the 
Process happens in the process execution engine. In case of connections to third party API's during process execution, 
an HTTP service worker can be employed to make the actual API requests and publish the results back into the process 
execution engine. The front-end web application polls the state of the running process in the engine via the front-end 
webserver and the Ketting API behind that.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2

!include DEVICONS/vuejs.puml
!include DEVICONS/nodejs.puml
!include DEVICONS/nestjs.puml

title Component Diagram for Ketting

System_Boundary(c1, "frontend.ketting.io"){
    Container(process_execution_web_application, "Process Execution Web Application", "JavaScript, Vue.js, Vuetify", "Provides process execution functionality to anonymous users", "vuejs")
    
    Component(app_controller, "App Controller", "NestJS", "Executes API calls to the backend service", "nestjs")
    
    ContainerDb(session_store, "Key-Value Store", "Redis", "Stores the SSI wallet ID that is used in any running process instance", "redis")
}

System_Boundary(c2, "admin.ketting.io"){
    Container(ketting_api, "Ketting API", "Node.JS, Strapi", "Web API that handles process data", "nodejs")
    
    Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
}

Container(http_worker, "HTTP Service Worker", "Node.js", "Sends HTTP requests", "nodejs")

System_Ext(external_api, "External API", "Gateway to an external service that the process depends on")
System_Ext(ssi_wallet, "SSI Wallet", "Private credential store", "wallet")
System_Ext(service_ledger, "Service Ledger", "Private credential store")

Rel_R(process_execution_web_application, app_controller, "Makes API call", "JSON/HTTPS")
Rel_L(app_controller, process_execution_web_application, "Delivers", "JSON")
Rel_D(app_controller, ketting_api, "Uses")
Rel_U(app_controller, session_store, "Reads from and writes to", "port 6379")
Rel_U(app_controller, service_ledger, "Makes API call", "JSON/HTTPS")
Rel_R(app_controller, ssi_wallet, "Makes API call", "JSON/HTTPS")
Rel_R(app_controller, ssi_wallet, "Polls via webhook", "HTTPS")
Rel_L(ssi_wallet, app_controller, "Delivers", "JSON")
Rel_R(ketting_api, process_engine, "Makes API call", "JSON/HTTPS")
Rel_L(process_engine, ketting_api, "Delivers", "JSON")
Rel_L(http_worker, process_engine, "Polls/Reports", "HTTP")
Rel_R(http_worker, external_api, "Consumes", "HTTPS")

@enduml
```

## Process administration

### Defining a Process

After signing into the Strapi Administration User Interface (UI) using its built-in authentication and authorization mechanism,
Administration Users can manage Processes and the details of Processes. The Process definitions are persisted in a relational
database. The building blocks of Processes are Steps. Any number of Steps can be chained into a Process in any given order.
Steps can be reused across multiple Processes. Every Step is meant to contain a coherent set of decision and execution logic.
A step can contain zero or more:
 - Variables
 - Connections
 - Decision Tables
 - Conditions

Variables represent different kinds of input to the Process. Connections represent outgoing API calls from the Process to third
party APIs. Conditions represent logical expressions to terminate the Process early, i.e. break the chain, hence the name "Ketting",
which is Dutch for "chain". Decision Tables provide a compact way to execute complex sets of Conditions based on multiple Variables. 
These can be deployed to the Business Process Execution Engine in DMN format.
Connections can be enhanced with Headers for authorization and with mappings for data-minimization.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2

!include DEVICONS/vuejs.puml
!include DEVICONS/nodejs.puml

title Component Diagram for Ketting

System_Boundary(c1, "admin.ketting.io"){
    Component(process, "Process", "Node.JS, Strapi", "Allows admin user to create/edit processes in the administration web application", "nodejs")
    
    Component(step, "Step", "Node.JS, Strapi", "Allows admin user to create/edit steps in the administration web application", "nodejs")
    
    Component(variable, "Variable", "Node.JS, Strapi", "Allows admin user to create/edit variables in the administration web application", "nodejs")
    
    Component(connection, "Connection", "Node.JS, Strapi", "Allows admin user to create/edit connections in the administration web application", "nodejs")
    
    Component(condition, "Condition", "Node.JS, Strapi", "Allows admin user to create/edit conditions in the administration web application", "nodejs")
    
    Component(decision_table, "Decision Table", "Node.JS, Strapi", "Allows admin user to create/edit decision tables in the administration web application", "nodejs")
    
    Component(header, "Header", "Node.JS, Strapi", "Allows admin user to create/edit headers in the administration web application", "nodejs")
    
    Component(mapping, "Mapping", "Node.JS, Strapi", "Allows admin user to create/edit mappings in the administration web application", "nodejs")
}

Rel_D(process, step, "Contains")
Rel_D(step, variable, "Contains")
Rel_D(step, connection, "Contains")
Rel_D(step, condition, "Contains")
Rel_D(step, decision_table, "Contains")

Rel_D(connection, header, "Contains")
Rel_D(connection, mapping, "Contains")

@enduml
```

### Deploying a Process

Deployments of Processes can be created in the Deployment module of the Administration UI. When a Deployment is created,
the Process definition is transformed into the executable BPMN format and published to the Business Process Execution Engine through
an internal API call.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2

!include DEVICONS/vuejs.puml
!include DEVICONS/nodejs.puml

title Component Diagram for Ketting

System_Boundary(c1, "admin.ketting.io"){
    Container(ketting_api, "Ketting API", "Node.JS, Strapi", "Web API to handle processes", "nodejs")
    
    Container(administration_web_application, "Administration Web Application", "Node.JS, Strapi", "Provides process management functionality to administrator", "nodejs")
    
    Component(process_controller, "Process Controller", "Node.JS, Strapi", "Allows admin user to create/edit processes in the administration web application", "nodejs")
    
    Component(deployment_controller, "Deployment Controller", "Node.JS, Strapi", "Allows admin user to deploy BPMN to the business process execution engine", "nodejs")
    
    Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
}

ComponentDb(relational_database, "Relational Database", "PostgreSQL", "Stores processes, deployments, user informations, etc", "postgresql")

Rel_L(administration_web_application, ketting_api, "Exposes")
Rel_D(administration_web_application, process_controller, "Uses")
Rel_L(process_controller, deployment_controller, "Uses")
Rel_R(process_controller, relational_database, "Reads from and writes to", "port 5432")
Rel_D(deployment_controller, process_engine, "Makes API call")

@enduml
```

