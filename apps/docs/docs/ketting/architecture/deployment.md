---
id: deployment
title: Deployment Architecture
sidebar_label: Deployment
description: Ketting Deployment Architecture
slug: '/architecture/deployment'
---

The Ketting monorepo is automatically deployed to a Kubernetes cluster on Google Cloud Platform using GitOps workflow with Terraform in Gitlab.
Deployments are triggered by changes pushed to the version control repository. Deployments will only be triggerd for those services of which the 
source code has been updated.

## Process Administration

The Process Administration part of Ketting is deployed as illustrated below.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Deployment.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2
!define KUBERNETES https://raw.githubusercontent.com/plantuml/plantuml-stdlib/master/kubernetes/

!include DEVICONS/postgresql.puml
!include DEVICONS/vuejs.puml
!include DEVICONS/nodejs.puml
!include DEVICONS/java.puml

!include KUBERNETES/k8s-sprites-unlabeled-25pct.puml

title Deployment Diagram for Ketting

Person(administration_user, "Administration User")

Deployment_Node(administation_user_desktop, "Administration User's Computer", "Desktop or laptop"){
    Deployment_Node(administration_desktop_browser, "Internet Browser Window"){
        Container(administration_web_application, "Administration Web Application", "Node.JS, Strapi", "Provides process management functionality to administrator", "nodejs")
    }
}

Deployment_Node(plc, "Kubernetes cluster", "Any cloud hosting provider or on-premise"){
    Deployment_Node(backend, "Backend Service"){
        Container(web_application, "Administration Web Application", "Node.JS, Strapi", "Provides process management functionality to administrator", "nodejs")
        
        ContainerDb(relational_database, "Relational Database", "PostgreSQL", "Stores process definitions, process details, administration of users and permissions", "postgresql")
    }
    Deployment_Node(ingress, "Ingress Service"){
        Container(ingress_controller, "Ingress Controller", "nginx, GKE, AKS, EKS", "exposes HTTPS routes from outside the cluster to services within the cluster", "ing")
    }
    Deployment_Node(camunda, "Camunda Service"){
        Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
    }
}

Rel_R(administration_user, administration_web_application, "Manages processes", "HTTPS")

Rel_D(administration_web_application, ingress_controller, "Consumes", "HTTPS")

Rel_U(ingress_controller, administration_web_application, "Serves", "HTTPS")

Rel_D(ingress_controller, web_application, "Exposes route /admin", "HTTPS")
Rel_L(ingress_controller, process_engine, "Exposes route /camunda", "HTTPS")

Rel_R(web_application, relational_database, "Reads from and writes to", "port 5432")
Rel_U(web_application, process_engine, "Deploys BPMN & DMN", "HTTP")

@enduml
```

## Process Execution

The Process Execution part of Ketting is deployed as illustrated below.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Deployment.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2
!define KUBERNETES https://raw.githubusercontent.com/plantuml/plantuml-stdlib/master/kubernetes/

!include DEVICONS/postgresql.puml
!include DEVICONS/vuejs.puml
!include DEVICONS/nodejs.puml
!include DEVICONS/java.puml

!include KUBERNETES/k8s-sprites-unlabeled-25pct.puml

title Deployment Diagram for Ketting

Person(anonymous_user, "Anonymous User")

Deployment_Node(user_desktop, "User's Computer", "Desktop or laptop"){
    Deployment_Node(browser, "Internet Browser Window"){
        Container(process_execution_web_application, "Process Execution Web Application", "JavaScript, Vue.js, Vuetify", "Provides process execution functionality to anonymous users", "vuejs")
    }
}

Deployment_Node(plc, "Kubernetes cluster", "Any cloud hosting provider or on-premise"){
    Deployment_Node(backend, "Backend Service"){
        Container(administration_web_application, "Administration Web Application", "Node.JS, Strapi", "Provides process management functionality to administrator", "nodejs")
    }
    Deployment_Node(frontend, "Frontend Service"){
        Container(web_application, "Process Execution Web Application", "JavaScript, Vue.js, Vuetify", "Provides process execution functionality to anonymous users", "vuejs")
        Container(process_execution_service, "Process Execution Service", "NestJS", "Executes actions from the Web Application", "nodejs")
        ContainerDb(session_store, "Key-Value Store", "Redis", "Stores the SSI wallet ID that is used in any running process instance", "redis")
    }
    Deployment_Node(ingress, "Ingress Service"){
        Container(ingress_controller, "Ingress Controller", "nginx, GKE, AKS, EKS", "exposes HTTPS routes from outside the cluster to services within the cluster", "ing")
    }
    Deployment_Node(camunda, "Camunda Service"){
        Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
    }
    Deployment_Node(worker, "HTTP Worker Service"){
        Container(http_worker, "HTTP Service Worker", "Node.js", "Sends HTTP requests", "nodejs")
    }
}

Rel_R(anonymous_user, process_execution_web_application, "Instantiates a process", "HTTPS")

Rel_D(process_execution_web_application, ingress_controller, "Consumes", "HTTPS")

Rel_U(ingress_controller, process_execution_web_application, "Serves", "HTTPS")

Rel_D(ingress_controller, administration_web_application, "Exposes route /admin", "HTTPS")
Rel_R(ingress_controller, web_application, "Exposes route /", "HTTPS")

Rel_L(process_execution_service, web_application, "Exposes route /api", "HTTPS")
Rel_R(process_execution_service, session_store, "Reads from and writes to", "port 6379")

Rel_R(administration_web_application, process_engine, "Makes API call", "JSON/HTTPS")

Rel_R(http_worker, process_engine, "Polls/Report", "HTTP")

@enduml
```