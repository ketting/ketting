---
id: dynamic
title: Dynamic Architecture
sidebar_label: Dynamic
description: Ketting Dynamic Architecture
slug: '/architecture/dynamic'
---

This section provides a more detailed view on specific use cases for Anonymous Users of Ketting

## Instantiating a process

After an Anonymous User has selected a Process to execute using the Process Execution Web Application, its webserver will proxy
an unauthenticated API call to the Ketting API. The Ketting API will immediately request a new process instance to be started inside
the Business Process Execution Engine through an internal API call. The execution engine will return a unique id for that running process
to the Ketting API. Because that id is guessable, being a short number, the Ketting API creates a complex UUID for the process instance, wraps
the id of the execution engine with the UUID into a JSON Web Token (JWT) and returns the UUID and the token. The UUID is safe to use in URLs
of subsequent requests. Subsequent request are secured with the token in the Authorization header. The Ketting API will reject subsequent requests
with invalid tokens or with token content not matching the UUID in the request URL or body.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2

!include DEVICONS/nodejs.puml

title Dynamic Diagram for Ketting

Person_Ext(anonymous_user, "Anonymous User")

System_Boundary(c1, "frontend.ketting.io"){
    Component(frontend, "Frontend Component", "Node.JS, Vue.js, NestJS", "Allows user to view and interact with processes", "nodejs")
}

System_Boundary(c2, "admin.ketting.io"){
    Container(ketting_api, "Ketting API", "Node.JS, Strapi", "Web API that handles process and process instance data", "nodejs")
    
    Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
}

Rel_R(anonymous_user, frontend, "Selects a process")
Rel_R(frontend, ketting_api, "Proxies to /processInstance", "JSON/HTTPS")
Rel_L(ketting_api, frontend, "Generates JWT token derived from the process instance object", "JWT")
Rel_R(ketting_api, process_engine, "Makes API call", "JSON/HTTPS")
Rel_L(process_engine, ketting_api, "Delivers process instance object", "JSON")

@enduml
```

## Set process variable

### Set non-pallet process variable

When prompted, users can post data into a running Process by making an API request authenticated with the JWT of the Process they themselves initiated.
In the Business Process Execution Engine data will be correlated by the internal process instance id with the corresponding Message object.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2

!include DEVICONS/nodejs.puml

title Dynamic Diagram for Ketting

Person_Ext(anonymous_user, "Anonymous User")

System_Boundary(c1, "frontend.ketting.io"){
    Component(frontend, "Frontend Component", "Node.JS, Vue.js, NestJS", "Allows user to view and interact with processes", "nodejs")
}

System_Boundary(c2, "admin.ketting.io"){
    Container(ketting_api, "Ketting API", "Node.JS, Strapi", "Web API that handles variables", "nodejs")
    
    Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
}

Rel_R(anonymous_user, frontend, "Inputs a variable")
Rel_R(frontend, ketting_api, "Proxies to /variables with JWT access token and key", "JSON/HTTPS")
Rel_R(ketting_api, process_engine, "Makes API call", "JSON/HTTPS")

@enduml
```

### Set pallet process variable

Before process starts, user stores their credentials in Wallet SSI. Then they can share their credential after scanning
the QR code presented. Afterwards, NestJS will verify that credential against previously saved credential identifier, in
case there is any, and against Service Ledger. When it's verified, the same process will be executed as for the non-pallet
variable.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2

!include DEVICONS/nodejs.puml

title Dynamic Diagram for Ketting

Person_Ext(anonymous_user, "Anonymous User")

System_Ext(ssi_wallet, "SSI Wallet", "Private credential store", "wallet")
System_Ext(service_ledger, "Service Ledger", "Private credential store")

System_Boundary(c1, "frontend.ketting.io"){
    Component(frontend, "Frontend Component", "Node.JS, Vue.js, NestJS", "Allows user to view and interact with processes", "nodejs")
    
    ContainerDb(session_store, "Key-Value Store", "Redis", "Stores the SSI wallet ID that is used in any running process instance", "redis")
}

System_Boundary(c2, "admin.ketting.io"){
    Container(ketting_api, "Ketting API", "Node.JS, Strapi", "Web API that handles variables", "nodejs")
    
    Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
}

Rel_R(anonymous_user, frontend, "Scans QR code")
Rel_U(anonymous_user, ssi_wallet, "Discloses and stores credentials")

Rel_R(frontend, ketting_api, "Proxies to /variables with JWT access token and key", "JSON/HTTPS")
Rel_U(frontend, session_store, "Reads from and writes to", "port 6379")
Rel_D(frontend, service_ledger, "Makes API call", "JSON/HTTPS")

Rel_D(ssi_wallet, frontend, "Delivers", "JSON/HTTPS")

Rel_R(ketting_api, process_engine, "Makes API call", "JSON/HTTPS")

@enduml
```

## Get process variables / activities / status

Anonymous users interact with a running Process in the form of a dialogue. The state of Process execution in the engine is polled by the Process Exection
Web Application via its webserver that proxies the Ketting API. All of these API endpoints are protected by the JSON Web Token corresponding to the respective
running process.
Ketting API using three endpoints:
 - `/variables`
 - `/activities`
 - `/state`

The `/variables` endpoint is used to get a list of variables internal the Business Process Execution Engine. For example, the Process could make an API call the 
response of which is mapped to one single variable that is published back into the running Process. The `/variables` endpoint exposes will expose its value.
The `/activities` endpoint is used to get a list of BPMN elements that have been completed in the Business Process Execution Engine. The result of that API call is
correlated to the Process definition retrieved from Strapi. This helps to present the progress of Process execution to the user in a flexible way.
The `/state` endpoint returns whether the execution of the Process inside the Business Process Execution Engine has been completed or not. If it is completed, the 
Process Exection Web Application can stop polling the API for information about the respective Process instance.

```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2

!include DEVICONS/nodejs.puml

title Dynamic Diagram for Ketting

Person_Ext(anonymous_user, "Anonymous User")

System_Boundary(c1, "frontend.ketting.io"){
    Component(frontend, "Frontend Component", "Node.JS, Vue.js, NestJS", "Allows user to view and interact with processes", "nodejs")
}

System_Boundary(c2, "admin.ketting.io"){
    Container(ketting_api, "Ketting API", "Node.JS, Strapi", "Web API that handles process variables, activities, and status", "nodejs")
    
    Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")
}

Rel_L(frontend, anonymous_user, "Shows result")
Rel_R(frontend, ketting_api, "Proxies to /variables or /activities or /status with JWT access token and key", "JSON/HTTPS")
Rel_L(ketting_api, frontend, "Delivers variables/activities/status", "JSON")
Rel_R(ketting_api, process_engine, "Makes API call", "JSON/HTTPS")
Rel_L(process_engine, ketting_api, "Delivers variables/activities/status", "JSON")

@enduml
```