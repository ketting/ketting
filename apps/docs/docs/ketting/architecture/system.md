---
id: system
title: System Architecture
sidebar_label: System
description: Ketting System Architecture
slug: '/architecture/system'
---

Ketting has two target audiences: Administration Users who manage Processes and Anonymous Users who execute Processes.

## System Context

```plantuml
@startuml "techtribesjs"
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_TOP_DOWN()

title System Context diagram for Ketting

Person(administration_user, "Administration User", "Policy maker, civil servant, technician")
Person_Ext(anonymous_user, "Anonymous User", "Civilian, social worker, civil servant, technician")
System(ketting, "Ketting", "Manage and execute business processes")

Rel_R(administration_user, ketting, "Manages business processes")
Rel_L(anonymous_user, ketting, "Executes business processes")

@enduml
```
