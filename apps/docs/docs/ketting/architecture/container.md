---
id: container
title: Container Architecture
sidebar_label: Container
description: Ketting Containers Architecture
slug: '/architecture/container'
---

Ketting consists of several application containers. The source code of all application containers is bundled in a 
monorepo.  The core API of Ketting is implemented in Strapi. Strapi also provides the administration user interface 
(UI). The UI that is used by anonymous users is built in VueJS and is served from a NestJS webserver. The NestJS 
webserver listens to the credential data sent from SSI Wallet via webhook and saves those data in Redis alongside 
process execution data. This webserver also serves as an API proxy for the core Ketting API to enhance security of the 
system. Processes defined by Administration Users are stored in a PostgreSQL database. Processes that are ready for 
execution by anonymous users can be deployed to a process execution engine like Camunda BPM. For performance reasons, 
outgoing API connections from Processes running in Camunda are handled asynchronously by an HTTP service worker.

## Container Context

```plantuml
@startuml "ketting"
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons2
!include DEVICONS/postgresql.puml
!include DEVICONS/vuejs.puml
!include DEVICONS/nodejs.puml
!include DEVICONS/java.puml
!include DEVICONS/redis.puml

LAYOUT_TOP_DOWN()

title Container Context diagram for Ketting

Person_Ext(anonymous_user, "Anonymous User")
Person(administration_user, "Administration User")

System_Ext(external_api, "External API", "Gateway to an external service that the process depends on")
System_Ext(ssi_wallet, "SSI Wallet", "Private credential store", "wallet")

System_Boundary(c1, "ketting.io"){
    Container(administration_web_application, "Administration Web Application", "Node.JS, Strapi", "Provides process management functionality to administrator", "nodejs")
    
    Container(process_execution_web_server, "Process Execution Web Server", "NestJS", "Executes actions from the Web Application", "nodejs")
    Container(process_execution_web_application, "Process Execution Web Application", "JavaScript, Vue.js, Vuetify", "Provides process execution functionality to anonymous users", "vuejs")

    ContainerDb(relational_database, "Relational Database", "PostgreSQL", "Stores process definitions, process details, administration of users and permissions", "postgresql")
    ContainerDb(session_store, "Key-Value Store", "Redis", "Stores the SSI wallet ID that is used in any running process instance", "redis")

    Container(ketting_api, "Ketting API", "Node.JS, Strapi", "REST API for user management, process management and process execution", "nodejs")

    Container(process_engine, "Business Process Execution Engine", "Camunda BPM", "Executes BPMN representations of process definitions and DMN representations of decisions", "java")

    Container(http_worker, "HTTP Service Worker", "Node.js", "Sends HTTP requests", "nodejs")
}

Rel_L(anonymous_user, ssi_wallet, "Discloses and stores credentials")

Rel_D(anonymous_user, process_execution_web_application, "Visits", "HTTPS")
Rel_D(administration_user, administration_web_application, "Visits", "HTTPS")

Rel_R(process_execution_web_server, process_execution_web_application, "Serves")
Rel_D(process_execution_web_server, session_store, "Reads from and writes to", "port 6379")

Rel_D(ssi_wallet, process_execution_web_server, "Delivers", "JSON")

Rel_D(administration_web_application, relational_database, "Reads from and writes to", "port 5432")

Rel_D(process_execution_web_server, ketting_api, "Proxies", "HTTPS")
Rel_D(ketting_api, process_engine, "Consumes", "HTTP")

Rel_D(administration_web_application, ketting_api, "Exposes")

Rel_L(http_worker, process_engine, "Polls/Reports", "HTTP")
Rel_R(http_worker, external_api, "Consumes", "HTTPS")

@enduml
```
