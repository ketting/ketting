---
id: introduction
title: Introduction
sidebar_label: Introduction
description: Introduction to Ketting
slug: '/'
---

Ketting is an open-source software project aimed at user-friendly management and execution of business processes. 
Combined with modern tooling and easy to use applications, Ketting provides a solution to design business processes.
The fundamental concept of Ketting lies in processes consist of series of steps, which also comprise to a group of 
variables, connections, conditions, and decision tables that are feasible from the CMS.
