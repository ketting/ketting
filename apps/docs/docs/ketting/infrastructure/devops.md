---
id: devops
title: DevOps
sidebar_label: DevOps
description: DevOps
slug: '/infrastructure/devops'
---

DevOps is a term combined from the words Development and Operations that signifies as a set of practices that works 
to automate and integrate the processes between software development and IT Operation teams, so they can build, test, 
and release software effectively and more reliably.

## Docusaurus
Docusaurus is a static site generator. It provides an extensive documentation features while keeping the tools light
and easy to learn. Ketting uses docusaurus to build its documentation.

## Terraform
Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform allows
developer to write infrastructure as code and apply it to popular service providers.

The biggest advantage provided by Terraform which is enforced comprehensively by Ketting is that it keeps track of 
the real infrastructure in a state file. Terraform also provides a dynamic variable functionality which is heavily 
used by Ketting through Gitlab CI/CD.

## Gitlab
Git is a free and open source distributed version control system designed to handle everything from small to large 
projects with speed and efficiency. GitLab is a Git-based fully integrated platform for software development.

### Monorepo
In GitLab, files are stored in a repository. A repository is similar to how files are stored in a folder or directory 
on local computer. Ketting application source code is bundled in a single repository (monorepo) hosted by Gitlab.

### Gitlab CI/CD
In the modern day, DevOps practices involve Continuous Integration and Continuous Delivery. While Continuous Integration
lets code changes pushed by developers to be built and tested automatically, Continuous Delivery lets those code changes 
to be deployed in any given environment automatically.

Gitlab provides CI/CD functionalities. To harness these functionalities, a configuration file named `.gitlab-ci.yaml` 
should be created. Inside this configuration file various scripts can be added as jobs. Jobs will be executed by Gitlab 
Runner. These jobs by default will be run for each commit but in Ketting, certain rules have been exercised and thus,
when the rules are not met, these jobs will not be triggered.

Ketting uses Terraform to deploy all its services from the Gitlab CI/CD pipeline and save the current state of the
infrastructure inside Gitlab CI/CD cache. By keeping track of the infrastructure state, no changes will be applied
if the configuration file remains unchanged. By using Terraform, variables can be dynamic which means some
configurations can be stored in Gitlab CI/CD variables and be inserted using a command.

### Gitlab Pages
Gitlab pages allows static website to be deployed directly from a repository in Gitlab. To accomplish this task, all
files to be deployed has to be stored in a folder named public in the repository and a `.gitlab-ci.yaml` file has to 
be created with a specific job called deploy. This is what is used to deploy Ketting documentation.