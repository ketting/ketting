---
id: development
title: Development
sidebar_label: Development
description: Development
slug: '/infrastructure/development'
---

Ketting consists of multiple components grouped as one. Bearing that in mind, standardizing the development environment
is necessary to streamline the applications and services. To achieve the following tools are used:
- Docker
- Kubernetes
- PostgreSQL
- Skaffold
- Minikube

## Docker
Docker provides the ability to package and run an application in a loosely isolated environment called a container. 
The isolation and security allow you to run many containers simultaneously on a given host. Containers are lightweight 
and contain everything needed to run the application, so you do not need to rely on what is currently installed on the 
host. You can easily share containers while you work, and be sure that everyone you share with gets the same container 
that works in the same way. For more information please refer to their <a href="https://docs.docker.com/get-started/overview/">documentation</a>.

In the case of Ketting, docker is used to build images from commands that is specified in Dockerfiles added in each 
component (frontend, backend, camunda, and http service worker). These images will then later be run in containers which
will present the full Ketting application.

### Docker compose
Compose is a tool for defining and running multi-container Docker applications. With Compose, application's services
can be configured in a YAML file usually named as `docker-compose.yaml`. This is one way to run Ketting application.

## Kubernetes
Kubernetes, also known as K8s, is an open-source system for automating deployment, scaling, and management of 
containerized applications. For more information please refer to their <a href="https://kubernetes.io/">documentation</a>.

Kubernetes takes the containerized application (built from docker) and deploy it in a cluster of given platform 
specified in the configurations files that are created in yaml format. Once the application instance is created,
kubernetes will monitor it continuously. Furthermore, if the application instance is updated, then kubernetes will 
replace the instance with another instance from the cluster so there won't be any downtime.

### Kubectl
Kubectl is a command line interface used to create and manage Kubernetes deployment.

## Database
Database is a collection of data or information in an organized way. In computing, database is stored in a computing
system. There are multiple computing system available but for Ketting application, PostgreSQL is utilized.

### PostgreSQL
PostgreSQL is a powerful, open source object-relational database system that uses and extends the SQL language combined
with many features that safely store and scale the most complicated data workloads.

The backend system (Strapi) generates a database with all its components from the first run. Moreover, Strapi also
updates the tables and its structure based on the changes made in the UI. This simplifies the database setup in Strapi
since only database connection is left to be defined in the configuration.

By default, PostgreSQL is listening to port 5432 unless specified otherwise. The port along with the credentials and
connections to the backend service has to be detailed in a Kubernetes deployment file for PostgreSQL database to be
fully working and picked up by Skaffold later on.

## Minikube
Minikube lets Kubernetes cluster to be created in a local machine. For Minikube to work, a virtual machine environment
such as docker is required.

## Skaffold
Skaffold is a command line tool that facilitates continuous development for Kubernetes-native applications. Skaffold
allows local environment to mimic the environment presented in cloud services.

Prerequisites:
- Minikube installed and started
- Kubectl installed
- Docker installed
- Skaffold installed

For Ketting application, Skaffold takes docker, kubectl, and services configurations inside `skaffold.yaml` file.
By initializing and run Skaffold in the dev mode, it will build and deploy all artifacts specified in the
`skaffold.yaml` file. Upon successful build and deploy, Skaffold will start watching all source file dependencies for 
all artifacts specified in the project. As changes are made to these source files, Skaffold will rebuild the associated 
artifacts, and redeploy the new changes to the cluster.