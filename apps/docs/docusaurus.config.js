const simplePlantUML = require("@akebifiky/remark-simple-plantuml")

module.exports = {
  title: 'Ketting',
  tagline: 'Model and execute complex business processes as simple unidimensional chains',
  url: 'https://ketting.gitlab.io/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'ketting',
  projectName: 'ketting',
  themeConfig: {
    navbar: {
      title: 'Ketting',
      logo: {
        alt: 'Ketting Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/ketting/ketting',
          label: 'Gitlab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Introduction',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/ketting/ketting',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()}, Ketting. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [simplePlantUML],
          editUrl:
            'https://gitlab.com/ketting/ketting/-/tree/master/website/',
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com/ketting/ketting/-/tree/master/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
