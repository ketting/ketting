---
slug: project-update
title: Project update
author: Jesse van Muijden
author_title: Ketting Core Team
author_url: https://gitlab.com/jessevanmuijden
author_image_url: https://avatars0.githubusercontent.com/u/2759225?v=4
tags: [ketting, project]
---

The latest update about the project is that the project is being updated. The devops setup and the documentation will be improved considerably. In addition, the existing code base will be cleaned up and refactored.
