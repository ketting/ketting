import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import {
  mdiShieldAccount,
  mdiTransitConnectionVariant,
  mdiTuneVariant,
} from '@mdi/js';
import Icon from '@mdi/react';
import styles from './styles.module.css';

const features = [
  {
    title: 'Algorithmic Transparency',
    imageUrl: mdiTransitConnectionVariant,
    description: (
      <>
        Standardized business process modeling to clarify data flow and decision logic.
      </>
    ),
  },
  {
    title: 'Privacy by Design',
    imageUrl: mdiShieldAccount,
    description: (
      <>
        Never share more personal data than necessary for the current stage of processing.
      </>
    ),
  },
  {
    title: 'In Control',
    imageUrl: mdiTuneVariant,
    description: (
      <>
        Algorithmic transparency and privacy by design put you in control of your data.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imageUrl && (
        <div className="text--center">
          <Icon path={imageUrl}
            size={4}
            color="#6b8ec4"
          />
        </div>
      )}
      <h3 className="text--center">{title}</h3>
      <p className="text--center">{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title="Project Homepage"
      description="Model and execute complex business processes as simple unidimensional chains">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--secondary button--lg primary-text',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
