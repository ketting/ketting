const {
  Client,
  logger,
  Variables,
} = require('camunda-external-task-client-js');
const axios = require('axios');

const config = {
  baseUrl: process.env.CAMUNDA_URL || 'http://localhost:8080/engine-rest',
  use: logger
};

const client = new Client(config);

client.subscribe('http', async function({ task, taskService }) {
  try {
    const variables = task.variables.getAll();
    let body = variables.body || '';
    let path = variables.path || '';
    for (let name in variables) {
      body = body.replace(new RegExp(`{{${name}}}`, 'g'), variables[name]);
      path = path.replace(new RegExp(`{{${name}}}`, 'g'), variables[name]);
    }
    const config = {
      baseURL: variables.url,
      url: path,
      method: variables.method,
      headers: JSON.parse(variables.header),
    };
    if (variables.method.toLowerCase() === 'get') {
      config.params = JSON.parse(body);
    } else {
      config.data = JSON.parse(body);
    }
    const response = await axios(config);
    let data = response.data;
    while ('data' in data) {
      data = data.data;
    }
    taskService.complete(task, new Variables().setAll({json: data}));
  } catch (error) {
    console.log(error);
    taskService.handleFailure(task, {
      errorMessage: error.message,
      errorDetails: '',
      retries: 0
    });
  }
});
