import { Controller, Post, Body, BadRequestException } from '@nestjs/common';
import * as moment from 'moment/moment';
import { now } from 'moment/moment';

@Controller()
export class MainController {
  @Post('calculateAge')
  calculateAge(@Body() body: any): any {
    if (!body || !body.birthDate) {
      throw new BadRequestException('Missing birth date');
    }

    if (moment(body.birthDate, 'DD-MM-YYYY').isAfter(moment(now()))) {
      throw new BadRequestException('Invalid birth date');
    }

    return {
      age: parseInt(moment(body.birthDate, 'DD-MM-YYYY').fromNow(true)),
    };
  }
}
