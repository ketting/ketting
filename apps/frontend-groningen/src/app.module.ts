import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ConfigModule } from '@nestjs/config';
import { join } from 'path';
import { WalletController } from './wallet/wallet.controller';
import { WalletService } from './wallet/wallet.service';
import * as redis from 'redis';
import { MainController } from './main/main.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client/dist'),
    }),
  ],
  controllers: [AppController, WalletController, MainController],
  providers: [
    AppService,
    WalletService,
    {
      provide: redis,
      useValue: redis,
    },
  ],
})
export class AppModule {}
