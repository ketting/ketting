import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import { promisify } from 'util';
import { AppService } from '../app.service';
import * as redis from 'redis';
import { Inject } from '@nestjs/common';

@Injectable()
export class WalletService {
  static readonly PATH_SESSION_CREATE: string = '/api/v1/register';
  static readonly PATH_REQUEST_CREDENTIAL: string = '/api/v1/request';
  static readonly PATH_CREDENTIAL_VERIFICATION: string = '/credentials/verify';

  private readonly redisClient;

  constructor(
    private readonly configService: ConfigService,
    private appService: AppService,
    @Inject(redis) private readonly redis: redis,
  ) {
    this.redisClient = redis.createClient({
      host: this.configService.get<string>('REDIS_HOST') || 'localhost',
    });
    this.redisClient.hmget = promisify(this.redisClient.hmget);
  }

  async createWalletSession(id: string, Authorization: string): Promise<any> {
    const url =
      this.configService.get<string>('WALLET_BASE_URL') +
      WalletService.PATH_SESSION_CREATE;
    const { data } = await axios.post(url, {
      webhook: this.configService.get<string>('WEBHOOK_URL'),
    });
    this.redisClient.hmset(data.session, [
      'processInstanceId',
      id,
      'processInstanceToken',
      Authorization,
    ]);
    this.redisClient.expire(data.session, 60);
    return data;
  }

  async handleWalletWebhook(body: any): Promise<any> {
    const variableSlug = await this.getVariableSlug(body.session);
    if (!variableSlug) {
      return false;
    }

    const processVariable = await this.getProcessVariableBySlug(variableSlug);
    if (!processVariable) {
      return false;
    }

    if (body?.content?.verifiableCredential) {
      if (!(await this.verifyCredential(body))) {
        return false;
      }

      const variableName = this.normalizeVariableName(
        processVariable.title.toLowerCase(),
      );
      if (
        processVariable.source.toLowerCase() !== 'pallet' ||
        !(
          variableName in
          body?.content?.verifiableCredential[0].credentialSubject
        )
      ) {
        return false;
      }

      await this.submitCredential(
        body.session,
        variableSlug,
        body?.content?.verifiableCredential[0].credentialSubject[variableName],
        processVariable.type === 'number' ? 'Integer' : processVariable.type,
      );

      return body;
    }

    await this.requestCredential({
      session: body.session,
      issuer: processVariable.issuer,
      schemaId: processVariable.schemaId,
    });

    return body;
  }

  async getProcessVariableBySlug(slug: string): Promise<any> {
    try {
      const data = await axios.get('/variables', {
        headers: {
          Authorization: `Bearer ${await this.appService.getToken()}`,
        },
        params: { slug: slug },
      });
      return data.data[0];
    } catch (e) {
      console.log(e);
    }
  }

  async getVariableSlug(sessionId: string): Promise<any> {
    try {
      const activities = await this.appService.getProcessInstanceActivities(
        (await this.redisClient.hmget(sessionId, 'processInstanceId')).pop(),
        (await this.redisClient.hmget(sessionId, 'processInstanceToken')).pop(),
      );
      const activity = activities[activities.length - 1];
      return activity.type === 'intermediateMessageCatch' ? activity.name : '';
    } catch (e) {
      console.log(e);
    }
  }

  normalizeVariableName(variableName: string) {
    if (!variableName.includes('_')) {
      return variableName;
    }

    let normalizedVariableName = '';
    variableName.split('_').forEach(function (element, index) {
      normalizedVariableName +=
        index === 0 ? element : element[0].toUpperCase() + element.slice(1);
    });
    return normalizedVariableName;
  }

  async submitCredential(
    sessionId: string,
    key: string,
    value: any,
    type: string,
  ) {
    try {
      await this.appService.putProcessInstanceVariables(
        (await this.redisClient.hmget(sessionId, 'processInstanceId')).pop(),
        { variables: { [key]: { value, type } } },
        (await this.redisClient.hmget(sessionId, 'processInstanceToken')).pop(),
      );
    } catch (e) {
      console.log(e);
    }
  }

  async requestCredential(body: any): Promise<any> {
    const url =
      this.configService.get<string>('WALLET_BASE_URL') +
      WalletService.PATH_REQUEST_CREDENTIAL;
    try {
      const { data } = await axios.post(url, {
        session: body.session,
        data: {
          did: body.issuer,
          credentialSchema: JSON.stringify([body.schemaId]),
          type: 'request',
        },
      });
      return data;
    } catch (e) {
      console.log(e);
    }
  }

  async verifyCredential(body: any): Promise<boolean> {
    let credentialSubjectId = (
      await this.redisClient.hmget(body.session, 'credentialSubjectId')
    ).pop();
    if (!credentialSubjectId) {
      this.redisClient.hmset(
        body.session,
        'credentialSubjectId',
        body?.content?.verifiableCredential[0].credentialSubject.id,
      );
    }

    credentialSubjectId = (
      await this.redisClient.hmget(body.session, 'credentialSubjectId')
    ).pop();
    if (
      credentialSubjectId !==
      body?.content?.verifiableCredential[0].credentialSubject.id
    ) {
      return false;
    }

    try {
      const url =
        this.configService.get<string>('SERVICE_LEDGER_BASE_URL') +
        WalletService.PATH_CREDENTIAL_VERIFICATION;
      const { data } = await axios.post(
        url,
        body?.content?.verifiableCredential[0],
      );
      return data.verified;
    } catch (e) {
      console.log(e);
    }
  }
}
