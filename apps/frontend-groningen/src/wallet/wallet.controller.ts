import { Body, Controller, Post, Headers, Param } from '@nestjs/common';
import { WalletService } from './wallet.service';

@Controller()
export class WalletController {
  constructor(private readonly walletService: WalletService) {}

  @Post('processInstances/:id/walletSession')
  createWalletSession(
    @Param('id') id: string,
    @Headers('Authorization') Authorization: string,
  ): Promise<any> {
    return this.walletService.createWalletSession(id, Authorization);
  }

  @Post('wallet/callback')
  handleWebhook(@Body() body: any): Promise<any> {
    return this.walletService.handleWalletWebhook(body);
  }
}
