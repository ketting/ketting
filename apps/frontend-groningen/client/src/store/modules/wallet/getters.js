export default {
  sessionId: (state) => state.sessionId,
  qrCodeImage: (state) => state.qrCodeImage,
}
