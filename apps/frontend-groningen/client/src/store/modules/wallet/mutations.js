export default {
  setSessionId (state, payload) {
    state.sessionId = payload
  },
  setQrCodeImage (state, payload) {
    state.qrCodeImage = payload
  },
}
