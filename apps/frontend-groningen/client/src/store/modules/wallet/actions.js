import axios from "axios";

export default {
  async createSession ({commit}, payload) {
    try {
      const {data} = await axios.post(`/processInstances/${payload.key}/walletSession`, '', {
        headers: { Authorization: `Bearer ${payload.token}` },
      });
      commit('setSessionId', data.session);
      commit('setQrCodeImage', data.qr);
      return data;
    } catch (e) {
      console.log('error: ', e);
    }
  },
  clearWalletData ({commit}) {
    commit('setSessionId', '')
    commit('setQrCodeImage', '')
  },
}