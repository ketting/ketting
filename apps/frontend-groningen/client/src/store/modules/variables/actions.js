export default {
  async setInputVariables ({commit}, payload) {
    Object.keys(payload).forEach(key => {
      commit('setInputVariable', {key, value: payload[key]})
    })
  },

  async updateOutputVariables ({commit}, payload) {
    commit('setOutputVariables', Object.keys(payload).map(key => ({key, value: payload[key]})))
  },
}
