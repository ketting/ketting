export default {
  inputVariables: (state) => state.inputVariables,
  outputVariables: (state) => state.outputVariables,
}
