export default {
  setInputVariable (state, payload) {
    const index = state.inputVariables.findIndex((inputVariable => inputVariable.key === payload.key))
    if (index >= 0) {
      state.inputVariables.splice(index, 1)
    }
    state.inputVariables.push(payload)
  },
  setOutputVariables (state, payload) {
    state.outputVariables = payload
  },
}
