export default {
  setProcesses (state, payload) {
    state.processes = payload
  },
  setProcessInstanceToken (state, payload) {
    state.processInstanceToken = payload
  },
  setProcessInstanceKey (state, payload) {
    state.processInstanceKey = payload
  },
  setRunningProcess (state, payload) {
    state.runningProcess = payload
  },
  setProcessInstanceVariables (state, payload) {
    state.processInstanceVariables = payload
  },
  setProcessInstanceActivities (state, payload) {
    state.processInstanceActivities = payload
  },
  setProcessInstanceStatus (state, payload) {
    state.processInstanceStatus = payload
  },
  setResult (state, result) {
    const resultIndex = state.results.findIndex(r => r.slug === result.slug)
    if (resultIndex >= 0) {
      state.results[resultIndex] = result
    } else {
      state.results.push(result)
    }
  },

  setMatchingProcesses (state, processes) {
    state.matchingProcesses = processes
  },

  addMatchingProcess (state, slug) {
    if (! state.matchingProcesses.includes(slug)) {
      state.matchingProcesses.push(slug)
    }
  },
}
