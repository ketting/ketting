export default {
  matchingProcesses: [],
  processes: [],
  processInstanceToken: null,
  processInstanceKey: null,
  runningProcess: null,
  processInstanceVariables: {},
  processInstanceActivities: [],
  processInstanceStatus: null,
  results: [],
}
