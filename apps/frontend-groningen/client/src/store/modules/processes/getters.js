export default {
  filteredProcesses: (state) => {
    return state.matchingProcesses.length
      ? state.processes.filter(p => state.matchingProcesses.includes(p.slug))
      : state.processes
  },
  processes: (state) => state.processes,
  processInstanceToken: (state) => state.processInstanceToken,
  processInstanceKey: (state) => state.processInstanceKey,
  runningProcess: (state) => state.runningProcess,
  processInstanceVariables: (state) => state.processInstanceVariables,
  processInstanceActivities: (state) => state.processInstanceActivities,
  processInstanceStatus: (state) => state.processInstanceStatus,
  results: (state) => state.results,
  resultBySlug: (state) => (slug) => state.results.find(result => result.slug === slug),
  processBySlug: (state) => (slug) => state.processes.find(process => process.slug === slug),
  matchingProcesses: (state) => state.matchingProcesses,
}
