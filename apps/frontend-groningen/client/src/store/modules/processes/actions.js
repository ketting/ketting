import axios from 'axios'

export default {
  async fetchProcesses ({commit}) {
    try {
      const {data} = await axios.get('/processes')
      const processes = data.data.map(p => {
        let index = 0
        p.steps = p.steps.map(s => {
          const step = s.step
          step.variables = step.variables.map(v => {
            v.variable.index = index++
            return v.variable
          })
          step.connections = step.connections.map(c => {
            c.connection.index = index++
            return c.connection
          })
          step.decisionTables = step.decisionTables.map(d => {
            d.decisionTable.index = index++
            return d.decisionTable
          })
          step.conditions = step.conditions.map(c => {
            c.condition.index = index++
            return c.condition
          })
          return step
        })

        p.variables = p.steps.reduce((previous, current) => [...previous, ...current.variables], [])
        p.connections = p.steps.reduce((previous, current) => [...previous, ...current.connections], [])
        p.decisionTables = p.steps.reduce((previous, current) => [...previous, ...current.decisionTables], [])
        p.conditions = p.steps.reduce((previous, current) => [...previous, ...current.conditions], [])

        return p
      })
      commit('setProcesses', processes)
    } catch (e) {
      console.log(e)
    }
  },

  async clearProcessInstance ({commit, dispatch}) {
    dispatch('setProcessInstanceVariables', {})
    dispatch('setProcessInstanceActivities', [])
    dispatch('setProcessInstanceStatus', null)
    commit('setRunningProcess', null)
    commit('setProcessInstanceToken', null)
    commit('setProcessInstanceKey', null)
  },

  async instantiateProcess ({commit, dispatch}, p) {
    dispatch('setProcessInstanceVariables', {})
    dispatch('setProcessInstanceActivities', [])
    dispatch('setProcessInstanceStatus', null)
    try {
      const {data} = await axios.post('/processInstances', {
        bpmnProcessId: p.slug,
      })
      commit('setRunningProcess', p)
      commit('setProcessInstanceToken', data.accessToken)
      commit('setProcessInstanceKey', data.workflowInstanceKey)
    } catch (e) {
      console.log(e)
    }
  },

  async fetchProcessInstanceVariables ({dispatch}, payload) {
    try {
      const {data} = await axios.get(`/processInstances/${payload.key}/variables`, {
        headers: { Authorization: `Bearer ${payload.token}` },
      })
      dispatch('setProcessInstanceVariables', data)
    } catch (e) {
      console.log(e)
    }
  },

  async fetchProcessInstanceActivities ({dispatch}, payload) {
    try {
      const {data} = await axios.get(`/processInstances/${payload.key}/activities`, {
        headers: { Authorization: `Bearer ${payload.token}` },
      })
      dispatch('setProcessInstanceActivities', data)
    } catch (e) {
      console.log(e)
    }
  },

  async fetchProcessInstanceStatus ({dispatch}, payload) {
    try {
      const {data} = await axios.get(`/processInstances/${payload.key}/status`, {
        headers: { Authorization: `Bearer ${payload.token}` },
      })
      dispatch('setProcessInstanceStatus', data.state)
    } catch (e) {
      console.log(e)
    }
  },

  async submitVariables (context, payload) {
    console.log('payload', payload);
    try {
      await axios.put(
        `/processInstances/${payload.key}/variables`,
          payload.data,
        { headers: { Authorization: `Bearer ${payload.token}` } }
      )
    } catch (e) {
      console.log(e)
    }
  },

  async updateAttribute ({getters, dispatch}, process) {
    if (!process) {
      return;
    }

    await dispatch('fetchProcessInstanceVariables', {
      token: getters.processInstanceToken,
      key: getters.processInstanceKey,
    });

    for (const key in getters.processInstanceVariables) {
      const processVariable = process.variables.find(variable => variable.slug === key);
      if (!processVariable) {
        return;
      }

      dispatch('setAttribute', {
        title: processVariable.title,
        slug: key,
        value: getters.processInstanceVariables[key],
      });
    }
  },

  setProcessInstanceVariables ({commit}, variables) {
    commit('setProcessInstanceVariables', variables)
  },

  setProcessInstanceActivities ({commit}, activities) {
    commit('setProcessInstanceActivities', activities)
  },

  setProcessInstanceStatus ({commit}, status) {
    commit('setProcessInstanceStatus', status)
  },

  setResult ({commit}, result) {
    commit('setResult', result)
  },

  clearMatchingProcesses ({commit}) {
    commit('setMatchingProcesses', [])
  },

  addMatchingProcess ({commit}, slug) {
    commit('addMatchingProcess', slug)
  },
}
