import attributes from './attributes'
import common from './common'
import processes from './processes'
import progress from './progress'
import variables from './variables'
import wallet from './wallet'

export default {
  attributes,
  common,
  processes,
  progress,
  variables,
  wallet,
}