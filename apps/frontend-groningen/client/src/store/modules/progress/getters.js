export default {
  currentProgress: (state) => state.currentProgress,
  totalProgress: (state) => state.totalProgress,
}