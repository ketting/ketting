export default {
  setTotalProgress (state, payload) {
    state.totalProgress = payload
  },
  setCurrentProgress (state, payload) {
    state.currentProgress = payload
  }
}