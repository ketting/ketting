export default {
  setCurrentProgress ({commit}, payload) {
    commit('setCurrentProgress', payload)
  },
  setTotalProgress ({commit}, payload) {
    commit('setTotalProgress', payload)
  },
}