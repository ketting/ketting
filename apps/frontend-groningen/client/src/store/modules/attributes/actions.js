export default {
  clearAttributes ({commit}) {
    commit('setAttributes', [])
  },

  setAttribute ({commit}, variable) {
    commit('setAttribute', variable)
  },
}
