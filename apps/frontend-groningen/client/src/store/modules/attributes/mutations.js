export default {
  setAttribute (state, variable) {
    const attributeIndex = state.attributes.findIndex(attribute => attribute.slug === variable.slug)
    if (attributeIndex >= 0) {
      state.attributes[attributeIndex].value = variable.value
    } else {
      state.attributes.push(variable)
    }
  },
  setAttributes (state, payload) {
    state.attributes = payload
  },
}
