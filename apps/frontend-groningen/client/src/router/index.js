import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import HistoryView from '../views/HistoryView.vue'
import attributes from './attributes'
import check from './check'
import result from './result'
import zoekhulp from './zoekhulp'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/:slug/audit',
    name: 'History',
    component: HistoryView
  },
  ...attributes,
  ...check,
  ...result,
  ...zoekhulp,
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to) {
    if (to.hash) {
      return {
        selector: to.hash,
        behavior: 'smooth',
      }
    }
    return {
      x: 0,
      y: 0,
    }
  }
})

export default router
