import AttributeView from '../../views/AttributeView.vue'

export default [
  {
    path: '/gegevens',
    name: 'Attributes',
    component: AttributeView
  },
]