import CheckView from '../../views/CheckView.vue'
import CheckEverythingView from '../../views/CheckEverythingView.vue'

export default [
  {
    path: '/check/:slug',
    name: 'Check',
    component: CheckView
  },
  {
    path: '/minima-regelingen',
    name: 'CheckEverything',
    component: CheckEverythingView
  },
]