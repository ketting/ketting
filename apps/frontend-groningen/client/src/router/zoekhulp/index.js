import ZoekhulpView from '../../views/ZoekhulpView.vue'
import ZoekView from '../../views/ZoekView.vue'

export default [
  {
    path: '/zoekhulp',
    name: 'Zoekhulp',
    component: ZoekhulpView
  },
  {
    path: '/zoekhulp/:slug',
    name: 'ZoekhulpStep',
    component: ZoekhulpView
  },
  {
    path: '/zoeken',
    name: 'Zoeken',
    component: ZoekView
  },
]