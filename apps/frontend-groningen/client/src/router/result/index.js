import CheckResultView from '../../views/CheckResultView.vue'

export default [
  {
    path: '/resultaat/:slug',
    name: 'CheckResult',
    component: CheckResultView
  },
]