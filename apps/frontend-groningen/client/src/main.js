import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo)

if (! window.webpackHotUpdate) {
  store.dispatch('setEnvironment', 'prod')
  axios.defaults.baseURL = '/api';
} else {
  axios.defaults.baseURL = 'http://localhost:3001/api';
}


Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
