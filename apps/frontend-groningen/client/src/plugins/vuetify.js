import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#e60004', // '#e60004',
        secondary: '#4E7D96', // '#BF7717',
        tertiary: '#42944D',
        quaternary: '#1947AA',
        quinary: '#C92D25',
        background: '#F0F5F9',
      },
    },
    options: {
      customProperties: true,
    },
  },
});
