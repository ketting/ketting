#!/usr/bin/env bash

if [ "${NODE_ENV}" = "development" ]; then
  yarn develop
else
  yarn build
  yarn start
fi
