'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

const CamSDK = require('camunda-bpm-platform/webapps/camunda-bpm-sdk-js');

async function deploy(d) {
  const client = new CamSDK.Client({
    mock: false,
    apiUri: process.env.CAMUNDA_URL,
  });
  const deploymentService  = new client.resource('deployment');

  try {
    const w = {
      files: [{
        content: d.DMN,
        name: `${d.slug}.dmn`,
      }],
      deploymentName: d.slug,
      enableDuplicateFiltering: true,
      deploymentSource: 'process application'
    };
    const result = await deploymentService.create(w)
    if (! result) {
      throw new Error('Deployment has not been updated.');
    }
    const deployedDecisionDefinitions = Object.values(result.deployedDecisionDefinitions || {});
    for (let deployedDecisionDefinition of deployedDecisionDefinitions) {
      return {
        externalId: deployedDecisionDefinition.deploymentId,
        version: deployedDecisionDefinition.version,
      }
    }
  } catch (e) {
    throw strapi.errors.badRequest('Deployment to process engine failed: ' + e.message);
  }
}

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      if (! data['decision_table']) {
        throw strapi.errors.badRequest('Cannot create decision deployment without a decision table');
      }
      const dec = await strapi.services['decision-table'].findOne({id: data['decision_table']});
      const d = await deploy(dec);
      data.externalId = d.externalId;
      data.version = d.version;
    },
    async beforeUpdate(params, data) {
      if (! data['decision_table']) {
        throw strapi.errors.badRequest('Cannot create decision deployment without a decision table');
      }
      const dec = await strapi.services['decision-table'].findOne({id: data['decision_table']});
      const d = await deploy(dec);
      data.externalId = d.externalId;
      data.version = d.version;
    },
  },
};
