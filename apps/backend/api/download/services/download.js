const bpmnTransformer = require('@ovrhd/camunda-bpmn-transformer').default;

module.exports = {
  download: async (data) => {
    const p = await strapi.services.process.findOne({id: data.data});
    const steps = [];
    for (let s of p.steps) {
      const step = {
        variables: [],
        tasks: [],
        conditions: [],
        decisions: [],
      }
      const st = await strapi.services.step.findOne({id: s.step.id});
      for (let v of st.variables) {
        step.variables.push({
          name: v.variable.slug,
          source: v.variable.source,
          type: v.variable.type,
        });
      }
      for (let conn of st.connections) {
        const c = await strapi.services.connection.findOne({id: conn.connection.id});
        const headers = {};
        for (let h of c.headers) {
          headers[h.key] = h.value;
        }
        step.tasks.push({
          name: c.title,
          url: c.url,
          method: c.method.toUpperCase(),
          header: JSON.stringify(headers),
          path: c.path,
          body: JSON.stringify(c.body),
          output: c.mappings.map(mapping => {
            return {
              source: mapping.input,
              target: mapping.output,
            };
          }),
        });
      }
      for (let cond of st.conditions) {
        step.conditions.push({
          name: cond.condition.title,
          expression: cond.condition.expression,
          direction: cond.condition.outcome === 'Negative' ? 'left' : 'right',
        });
      }
      for (let dec of st.decisionTables) {
        step.decisions.push({
          name: dec.decisionTable.title,
          resultVariableName: 'result',
          decisionRef: dec.decisionTable.slug,
        });
      }
      steps.push(step);
    }
    const requiredData = {
      id: p.slug,
      name: p.title,
      steps: steps
    }

    return await bpmnTransformer.toBPMN(requiredData);
  }
};
