'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
module.exports = {
  async index(ctx) {
    const { id } = ctx.params;

    const result = await strapi.services.download.download({data: id});
    return JSON.stringify(result);
  },
};
