const CamSDK = require('camunda-bpm-platform/webapps/camunda-bpm-sdk-js');
const client = new CamSDK.Client({
  mock: false,
  apiUri: process.env.CAMUNDA_URL,
});
const uuid = require('uuid');
const jwt = require('jsonwebtoken');
const { getAbsoluteServerUrl } = require('strapi-utils');

module.exports = {
  createWorkflowInstance: async (data) => {
    const processDefinitionService  = new client.resource('process-definition');

    try {
      const correlationKey = uuid();

      const processInstance = await processDefinitionService.start({
        key: data.data,
        businessKey: correlationKey,
      });

      const workflowInstanceKey = processInstance.id;

      const expirationDateTime = new Date();
      expirationDateTime.setHours(expirationDateTime.getHours() + 1);

      const accessToken = jwt.sign(
        {
          jti: workflowInstanceKey,
          sub: correlationKey,
          exp: Math.floor(expirationDateTime / 1000)
        },
        strapi.plugins['users-permissions'].config.jwtSecret
      );

      return {
        accessToken: accessToken,
        workflowInstanceKey: workflowInstanceKey
      };
    } catch (e) {
      throw strapi.errors.badRequest(e.message);
    }
  },
  getWorkflowInstanceStatus: async (data) => {
    const historyService  = new client.resource('history');
    let processInstances = [];
    try {
      processInstances = await historyService.processInstance({
        processInstanceId: data.data,
        processInstanceBusinessKey: data.correlationKey
      });
    } catch (e) {
      throw strapi.errors.serverError(e.message);
    }

    if (processInstances.length === 0) {
      throw strapi.errors.notFound();
    }

    if (processInstances.length > 1) {
      throw strapi.errors.badRequest();
    }

    const processInstance = processInstances.pop();
    return {
      state: processInstance.state
    }
  },
  editVariable: async (data) => {
    const messageService  = new client.resource('message');
    try {
      return await messageService.correlate({
        messageName: Object.keys(data.data).shift() || uuid(),
        processVariables: data.data,
        businessKey: data.correlationKey
      });
    } catch (e) {
      throw strapi.errors.badRequest(e.message);
    }
  },
  getVariables: async (data) => {
    const historyService  = new client.resource('history');
    let variables = [];
    try {
      variables = await historyService.variableInstance({
        processInstanceId: data.data
      });
    } catch (e) {
      throw strapi.errors.serverError(e.message);
    }
    const response = {};
    variables.forEach(variable => response[variable.name] = variable.value)
    return response;
  },
  getActivities: async (data) => {
    const historyService  = new client.resource('history');
    let activities = [];
    try {
      activities = await historyService.activityInstance({
        processInstanceId: data.data
      });
    } catch (e) {
      throw strapi.errors.serverError(e.message);
    }
    return activities.map(activity => ({
      id: activity.activityId,
      name: activity.activityName,
      startTime: activity.startTime,
      endTime: activity.endTime,
      type: activity.activityType,
    })).sort((a, b) => new Date(a.startTime) < new Date(b.startTime) ? -1 : 1);
  },
  getProcesses: async () => {
    const result = await strapi.query('process').find({}, [
      'image',
      'steps.step',
      'steps.step.variables.variable',
      'steps.step.connections.connection',
      'steps.step.connections.connection.headers',
      'steps.step.connections.connection.mappings',
      'steps.step.decisionTables.decisionTable',
      'steps.step.conditions.condition',
    ]);

    let host = getAbsoluteServerUrl(strapi.config, true);
    if (process.env.NODE_ENV === 'production') {
      host = 'https://storage.googleapis.com' + process.env.GCS_BUCKET_NAME;
    }

    result.forEach(function(process) {
      if (process.image === null) {
        return;
      }
      process.image.url = host + process.image.url;
    });

    return result;
  }
};
