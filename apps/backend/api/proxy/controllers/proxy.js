'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
module.exports = {
  async processes() {
    const result = await strapi.services.proxy.getProcesses();
    return {data: result};
  },
  async create(ctx) {
    const { bpmnProcessId } = ctx.request.body;

    const result = await strapi.services.proxy.createWorkflowInstance({data: bpmnProcessId});
    return {data: result};
  },
  async status(ctx) {
    const { workflowInstanceId } = ctx.params;
    const { correlationKey } = ctx.request.header;

    const result = await strapi.services.proxy.getWorkflowInstanceStatus({data: workflowInstanceId, correlationKey});
    return {data: result};
  },
  async editVariable(ctx) {
    const { variables } = ctx.request.body;
    const { correlationKey } = ctx.request.header;

    const result = await strapi.services.proxy.editVariable({data: variables, correlationKey});
    return {data: result};
  },
  async getVariables(ctx) {
    const { workflowInstanceId } = ctx.params;

    const result = await strapi.services.proxy.getVariables({data: workflowInstanceId});
    return {data: result};
  },
  async getActivities(ctx) {
    const { workflowInstanceId } = ctx.params;

    const result = await strapi.services.proxy.getActivities({data: workflowInstanceId});
    return {data: result};
  },
};
