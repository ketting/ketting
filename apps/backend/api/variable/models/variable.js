const slugify = require('slugify');

module.exports = {
  /**
   * Triggered before user creation.
   */
  lifecycles: {
    async beforeCreate(data) {
      if (data.title && data.type && data.source) {
        const slug = slugify(data.title, {
          replacement: '_',
          lower: true,
        });
        data.slug = `${data.source.toLowerCase()}_${data.type.toLowerCase()}___${slug}`;
      }
    },
    async beforeUpdate(params, data) {
      if (data.title && data.type && data.source) {
        const slug = slugify(data.title, {
          replacement: '_',
          lower: true,
        });
        data.slug = `${data.source.toLowerCase()}_${data.type.toLowerCase()}___${slug}`;
      }
    },
  },
};