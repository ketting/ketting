const CamSDK = require('camunda-bpm-platform/webapps/camunda-bpm-sdk-js');
const bpmnTransformer = require('@ovrhd/camunda-bpmn-transformer').default;

async function deploy(p) {
  const client = new CamSDK.Client({
    mock: false,
    apiUri: process.env.CAMUNDA_URL,
  });
  const deploymentService  = new client.resource('deployment');
  const steps = [];
  for (let s of p.steps) {
    const step = {
      variables: [],
      tasks: [],
      conditions: [],
      decisions: [],
    }
    const st = await strapi.services.step.findOne({id: s.step.id});
    for (let v of st.variables) {
      step.variables.push({
        name: v.variable.slug,
        source: v.variable.source,
        type: v.variable.type,
      });
    }
    for (let conn of st.connections) {
      const c = await strapi.services.connection.findOne({id: conn.connection.id});
      const headers = {};
      for (let h of c.headers) {
        headers[h.key] = h.value;
      }
      step.tasks.push({
        name: c.title,
        url: c.url,
        method: c.method.toUpperCase(),
        header: JSON.stringify(headers),
        path: c.path,
        body: JSON.stringify(c.body),
        output: c.mappings.map(mapping => {
          return {
            source: mapping.input,
            target: mapping.output,
          };
        }),
      });
    }
    for (let cond of st.conditions) {
      step.conditions.push({
        name: cond.condition.title,
        expression: cond.condition.expression,
        direction: cond.condition.outcome === 'Negative' ? 'left' : 'right',
      });
    }
    for (let dec of st.decisionTables) {
      step.decisions.push({
        name: dec.decisionTable.title,
        resultVariableName: 'result',
        decisionRef: dec.decisionTable.slug,
      });
    }
    steps.push(step);
  }
  const data = {
    id: p.slug,
    name: p.title,
    steps,
  };

  try {
    const w = {
      files: [{
        content: await bpmnTransformer.toBPMN(data),
        name: `${p.slug}.bpmn`,
      }],
      deploymentName: p.title,
      enableDuplicateFiltering: true,
      deploymentSource: 'process application'
    };
    const result = await deploymentService.create(w)
    if (! result) {
      throw new Error('Deployment has not been updated.');
    }
    const deployedProcessDefinitions = Object.values(result.deployedProcessDefinitions || {});
    for (let deployedProcessDefinition of deployedProcessDefinitions) {
      return {
        externalId: deployedProcessDefinition.deploymentId,
        version: deployedProcessDefinition.version,
      }
    }
  } catch (e) {
    throw strapi.errors.badRequest('Deployment to process engine failed: ' + e.message);
  }
}

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      if (! data.process) {
        throw strapi.errors.badRequest('Cannot create deployment without a Process');
      }
      const p = await strapi.services.process.findOne({id: data.process});
      const d = await deploy(p);
      data.externalId = d.externalId;
      data.version = d.version;
    },
    async beforeUpdate(params, data) {
      if (! data.process) {
        throw strapi.errors.badRequest('Cannot update deployment without a Process');
      }
      const p = await strapi.services.process.findOne({id: data.process});
      const d = await deploy(p);
      data.externalId = d.externalId;
      data.version = d.version;
    },
  },
};
