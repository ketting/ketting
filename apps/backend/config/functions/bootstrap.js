'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/concepts/configurations.html#bootstrap
 */

module.exports = async () => {
  const adminUser = await strapi.query('user', 'admin').findOne({email: 'admin@ketting.io'});
  if (!adminUser) {
    const result = await createAdminUser();
    console.log(result);
  }

  const apiUser = await strapi.query('user', 'users-permissions').findOne({email: 'groningen@ketting.io'});
  if (!apiUser) {
    const result = await createApiUser();
    console.log(result);
  }

  const witgoedregelingProcess = await strapi.query('process').findOne({title: 'Witgoedregeling'});
  if (!witgoedregelingProcess) {
    const result = await createWitgoedregeling();
    console.log(result);
  }

  const laptopregelingProcess = await strapi.query('process').findOne({title: 'Laptopregeling'});
  if (!laptopregelingProcess) {
    const result = await createLaptopregeling();
    console.log(result);
  }

  const individueleInkomenstoeslagProcess = await strapi.query('process').findOne({title: 'Individuele Inkomenstoeslag'});
  if (!individueleInkomenstoeslagProcess) {
    const result = await createIndividueleInkomenstoeslag();
    console.log(result);
  }

  const individueleStudietoeslagProcess = await strapi.query('process').findOne({title: 'Individuele Studietoeslag'});
  if (!individueleStudietoeslagProcess) {
    const result = await createIndividueleStudietoeslag();
    console.log(result);
  }

  const tegemoetkomingKostenKinderopvangProcess = await strapi.query('process').findOne({title: 'Tegemoetkoming Kosten Kinderopvang'});
  if (!tegemoetkomingKostenKinderopvangProcess) {
    const result = await createTegemoetkomingKostenKinderopvangProcess();
    console.log(result);
  }

  const studentreistoeslagProcess = await strapi.query('process').findOne({title: 'Studentreistoeslag'});
  if (!studentreistoeslagProcess) {
    const result = await createStudentreistoeslagProcess();
    console.log(result);
  }
};

async function createAdminUser() {
  let superAdminRole = await strapi.query('role', 'admin').findOne({code: 'strapi-super-admin'});
  if (!superAdminRole) {
    superAdminRole = await strapi.query('role', 'admin').create({
      name: 'Super Admin',
      code: 'strapi-super-admin',
      description: 'Super Admins can access and manage all features and settings.',
    });
  }

  const params = {
    username: 'admin',
    password: 'admin123',
    firstname: 'Admin',
    lastname: 'Example',
    email: 'admin@ketting.io',
    blocked: false,
    isActive: true,
    roles: superAdminRole.id
  };

  params.password = await strapi.admin.services.auth.hashPassword(params.password);
  return strapi.query('user', 'admin').create({...params});
}

async function createApiUser() {
  const authenticatedRole = await strapi.query('role', 'users-permissions').findOne({type: 'authenticated'});
  await strapi.query('permission', 'users-permissions').update(
    {
      role: authenticatedRole.id,
      type: 'application',
      controller: 'proxy',
      action: 'create'
    },
    {enabled: true}
  );
  await strapi.query('permission', 'users-permissions').update(
    {
      role: authenticatedRole.id,
      type: 'application',
      controller: 'proxy',
      action: 'processes'
    },
    {enabled: true}
  );
  await strapi.query('permission', 'users-permissions').update(
    {
      role: authenticatedRole.id,
      type: 'application',
      controller: 'variable',
      action: 'find'
    },
    {enabled: true}
  );

  const params = {
    username: 'api-user',
    password: 'Changem3',
    email: 'groningen@ketting.io',
    confirmed: true,
    role: authenticatedRole.id,
    provider: 'local',
  };

  params.password = await strapi.admin.services.auth.hashPassword(params.password);
  return strapi.query('user', 'users-permissions').create({...params});
}

async function createWitgoedregeling() {
  const woonSituatieVariableData = {
    title: 'Zoekhulp Woonsituatie',
    description: 'Zoekhulp Woonsituatie',
    type: 'string',
    source: 'Input'
  };
  const woonSituatieVariable = await strapi.query('variable').create(woonSituatieVariableData);

  const werkSituatieVariableData = {
    title: 'Zoekhulp Werksituatie',
    description: 'Zoekhulp Werksituatie',
    type: 'string',
    source: 'Input'
  };
  const werkSituatieVariable = await strapi.query('variable').create(werkSituatieVariableData);

  const afwijzingZoekhulpWitgoedregelingConditionData = {
    title: 'Afwijzing zoekhulp witgoedregeling',
    expression: '${input_string___zoekhulp_werksituatie == "student" && input_string___zoekhulp_woonsituatie == "alleenstaand"}',
    outcome: 'Negative'
  };
  const afwijzingZoekhulpWitgoedregelingCondition = await strapi.query('condition').create(afwijzingZoekhulpWitgoedregelingConditionData);

  const ZoekhulpWitgoedregelingStepData = {
    title: 'Zoekhulp Witgoedregeling',
    variables: [
      { variable: woonSituatieVariable.id },
      { variable: werkSituatieVariable.id },
    ],
    conditions: [{ condition: afwijzingZoekhulpWitgoedregelingCondition.id }],
  };
  const ZoekhulpWitgoedregelingStep = await strapi.query('step').create(ZoekhulpWitgoedregelingStepData);

  const ouderDan21JaarVariableData = {
    title: 'Ouder dan 21 jaar',
    description: 'Ouder dan 21 jaar',
    type: 'boolean',
    source: 'Input'
  };
  const ouderDan21JaarVariable = await strapi.query('variable').create(ouderDan21JaarVariableData);

  const afwijzingLeeftijdgrens21ConditionData = {
    title: 'Afwijzing leeftijdgrens 21',
    expression: '${input_boolean___ouder_dan_21_jaar == false}',
    outcome: 'Negative'
  };
  const afwijzingLeeftijdgrens21Condition = await strapi.query('condition').create(afwijzingLeeftijdgrens21ConditionData);

  const checkLeeftijdgrens21StepData = {
    title: 'Check leeftijdgrens 21',
    variables: [{ variable: ouderDan21JaarVariable.id }],
    conditions: [{ condition: afwijzingLeeftijdgrens21Condition.id }],
  };
  const checkLeeftijdgrens21Step = await strapi.query('step').create(checkLeeftijdgrens21StepData);

  const inLevenVariableData = {
    title: 'In leven',
    description: 'In leven',
    type: 'boolean',
    source: 'Input'
  };
  const inLevenVariable = await strapi.query('variable').create(inLevenVariableData);

  const gemeenteGroningenVariableData = {
    title: 'Gemeente Groningen',
    description: 'Gemeente Groningen',
    type: 'boolean',
    source: 'Input'
  };
  const gemeenteGroningenVariable = await strapi.query('variable').create(gemeenteGroningenVariableData);

  const afwijzingGemeenteGroningenConditionData = {
    title: 'Afwijzing Gemeente Groningen',
    expression: '${input_boolean___gemeente_groningen == false}',
    outcome: 'Negative'
  };
  const afwijzingGemeenteGroningenCondition = await strapi.query('condition').create(afwijzingGemeenteGroningenConditionData);

  const afwijzingInLevenConditionData = {
    title: 'Afwijzing in leven',
    expression: '${input_boolean___in_leven == false}',
    outcome: 'Negative'
  };
  const afwijzingInLevenCondition = await strapi.query('condition').create(afwijzingInLevenConditionData);

  const CheckInLevenStepData = {
    title: 'Check in leven',
    variables: [
      { variable: inLevenVariable.id },
    ],
    conditions: [
      { condition: afwijzingInLevenCondition.id },
    ],
  };
  const CheckInLevendStep = await strapi.query('step').create(CheckInLevenStepData);

  const CheckLevendInGroningenStepData = {
    title: 'Check levend in Groningen',
    variables: [
      { variable: gemeenteGroningenVariable.id },
    ],
    conditions: [
      { condition: afwijzingGemeenteGroningenCondition.id },
    ],
  };
  const CheckLevendInGroningenStep = await strapi.query('step').create(CheckLevendInGroningenStepData);

  const processData = {
    title: 'Witgoedregeling',
    description: 'Financiele hulp bij het vervangen van witgoed of de aankoop van nieuw witgoed.',
    steps: [
      { step: ZoekhulpWitgoedregelingStep.id },
      { step: checkLeeftijdgrens21Step.id },
      { step: CheckInLevendStep.id },
      { step: CheckLevendInGroningenStep.id },
    ],
  };
  return strapi.query('process').create(processData);
}

async function createLaptopregeling() {
  const gemeenteVariableData = {
    title: 'Gemeente',
    description: 'Gemeente',
    type: 'string',
    source: 'Pallet',
    schemaId: 'blob:dock:5D7QRdAFgK5NQZYZ73hDBXWAtJE22a9Qxu1c3jTsjVKu86Ae',
    issuer: 'did:dock:5GEntWKU7yp8a6MHv7bTkxqammEj3baVap78WHaAv8PkbgZA'
  };
  const gemeenteVariable = await strapi.query('variable').create(gemeenteVariableData);

  const alleenVoorGemeenteGroningenConditionData = {
    title: 'Alleen voor Gemeente Groningen',
    expression: '${pallet_string___gemeente != "Groningen"}',
    outcome: 'Negative'
  };
  const alleenVoorGemeenteGroningenCondition = await strapi.query('condition').create(alleenVoorGemeenteGroningenConditionData);

  const gemeenteGroningenCheckStepData = {
    title: 'Gemeente Groningen',
    variables: [{ variable: gemeenteVariable.id }],
    conditions: [{ condition: alleenVoorGemeenteGroningenCondition.id }],
  };
  const gemeenteGroningenCheckStep = await strapi.query('step').create(gemeenteGroningenCheckStepData);

  const leeftijdVariableData = {
    title: 'Leeftijd',
    description: 'Leeftijd',
    type: 'number',
    source: 'Pallet',
    schemaId: 'blob:dock:5GvXN4nSUWNnubqPqf85m1f8Ee7kqojwHuSTgf2N2DBe3oEX',
    issuer: 'did:dock:5GEntWKU7yp8a6MHv7bTkxqammEj3baVap78WHaAv8PkbgZA'
  };
  const leeftijdVariable = await strapi.query('variable').create(leeftijdVariableData);

  const studentLeeftijdgrensConditionData = {
    title: 'Student leeftijdgrens',
    expression: '${pallet_number___leeftijd > 21}',
    outcome: 'Negative'
  };
  const studentLeeftijdgrensCondition = await strapi.query('condition').create(studentLeeftijdgrensConditionData);

  const studentLeeftijdgrensCheckStepData = {
    title: 'Student leeftijdgrens check',
    variables: [{ variable: leeftijdVariable.id }],
    conditions: [{ condition: studentLeeftijdgrensCondition.id }],
  };
  const studentLeeftijdgrensCheckStep = await strapi.query('step').create(studentLeeftijdgrensCheckStepData);

  const opleidingVariableData = {
    title: 'Opleiding',
    description: 'Doet u voltijd vmbo, havo, vwo of vavo?',
    type: 'boolean',
    source: 'Input',
  };
  const opleidingVariable = await strapi.query('variable').create(opleidingVariableData);

  const opleidingVerkiesbaarheidConditionData = {
    title: 'Opleiding verkiesbaarheid',
    expression: '${input_boolean___opleiding == false}',
    outcome: 'Negative'
  };
  const opleidingVerkiesbaarheidCondition = await strapi.query('condition').create(opleidingVerkiesbaarheidConditionData);

  const opleidingVerkiesbaarheidCheckStepData = {
    title: 'Opleiding verkiesbaarheid check',
    variables: [{ variable: opleidingVariable.id }],
    conditions: [{ condition: opleidingVerkiesbaarheidCondition.id }],
  };
  const opleidingVerkiesbaarheidCheckStep = await strapi.query('step').create(opleidingVerkiesbaarheidCheckStepData);

  const processData = {
    title: 'Laptopregeling',
    description: 'De gemeente kan een bijdrage leveren aan de aankoop van een laptop als uw kind er voor school een nodig heeft.',
    steps: [
      { step: gemeenteGroningenCheckStep.id },
      { step: studentLeeftijdgrensCheckStep.id },
      { step: opleidingVerkiesbaarheidCheckStep.id },
    ],
  };
  return strapi.query('process').create(processData);
}

async function createIndividueleInkomenstoeslag() {
  const inkomenVariableData = {
    title: 'Inkomen',
    description: 'Inkomen',
    type: 'number',
    source: 'Input',
  };
  const inkomenVariable = await strapi.query('variable').create(inkomenVariableData);

  const geboortedatumVariableData = {
    title: 'Geboortedatum',
    description: 'Geboortedatum',
    type: 'string',
    source: 'Pallet',
    schemaId: 'blob:dock:5E9meALJpReXR8GyyY1wKB3aCp4LW4bKhnrnQQD6QQjJRx4n',
    issuer: 'did:dock:5GEntWKU7yp8a6MHv7bTkxqammEj3baVap78WHaAv8PkbgZA',
  };
  const geboortedatumVariable = await strapi.query('variable').create(geboortedatumVariableData);

  const leeftijdMappingData = {
    input: '${ json.prop("age").numberValue() }',
    output: 'leeftijd'
  };
  const leeftijdMapping = await strapi.query('mapping').create(leeftijdMappingData);

  const connectionData = {
    title: 'Geboortedatum',
    url: 'http://frontend:3001/api/calculateAge',
    method: 'post',
    body: '{ "birthDate": "{{pallet_string___geboortedatum}}" }',
    mappings: [leeftijdMapping.id]
  };
  const connection = await strapi.query('connection').create(connectionData);

  const conditionData = {
    title: 'Inkomen goedgekeurd',
    expression: '${result.approved == false}',
    outcome: 'Negative'
  };
  const condition = await strapi.query('condition').create(conditionData);

  const decisionTableData = {
    title: 'Inkomenstoeslagbeleid',
    DMN: `<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="https://www.omg.org/spec/DMN/20191111/MODEL/" xmlns:dmndi="https://www.omg.org/spec/DMN/20191111/DMNDI/" xmlns:dc="http://www.omg.org/spec/DMN/20180521/DC/" xmlns:camunda="http://camunda.org/schema/1.0/dmn" id="Definitions_1g0jsk2" name="DRD" namespace="http://camunda.org/schema/1.0/dmn" exporter="Camunda Modeler" exporterVersion="4.6.0">
  <decision id="inkomenstoeslagbeleid" name="Inkomenstoeslagbeleid">
    <decisionTable id="DecisionTable_1w44pcs" hitPolicy="FIRST">
      <input id="Input_1" camunda:inputVariable="leeftijd">
        <inputExpression id="InputExpression_1" typeRef="integer">
          <text>leeftijd</text>
        </inputExpression>
      </input>
      <input id="InputClause_1brrmh4" camunda:inputVariable="input_number___inkomen">
        <inputExpression id="LiteralExpression_0bi47n7" typeRef="integer">
          <text>input_number___inkomen</text>
        </inputExpression>
      </input>
      <output id="Output_1" name="approved" typeRef="boolean" />
      <rule id="DecisionRule_1mcqoy9">
        <inputEntry id="UnaryTests_1y02wh8">
          <text>&gt;= 21</text>
        </inputEntry>
        <inputEntry id="UnaryTests_19nmd8p">
          <text>&lt; 20000</text>
        </inputEntry>
        <outputEntry id="LiteralExpression_0eia0qs">
          <text>true</text>
        </outputEntry>
      </rule>
      <rule id="DecisionRule_1v87aw9">
        <inputEntry id="UnaryTests_0zusi1u">
          <text>&gt;= 21</text>
        </inputEntry>
        <inputEntry id="UnaryTests_068j6z9">
          <text>&gt;= 20000</text>
        </inputEntry>
        <outputEntry id="LiteralExpression_0ewdxx6">
          <text>false</text>
        </outputEntry>
      </rule>
      <rule id="DecisionRule_1meqnmm">
        <inputEntry id="UnaryTests_0im4amf">
          <text>&lt; 21</text>
        </inputEntry>
        <inputEntry id="UnaryTests_1jsmnw1">
          <text>&lt; 20000</text>
        </inputEntry>
        <outputEntry id="LiteralExpression_01rq2jj">
          <text>false</text>
        </outputEntry>
      </rule>
      <rule id="DecisionRule_193u0q4">
        <inputEntry id="UnaryTests_0ou7hjo">
          <text>&lt; 21</text>
        </inputEntry>
        <inputEntry id="UnaryTests_10rjsyj">
          <text>&gt;= 20000</text>
        </inputEntry>
        <outputEntry id="LiteralExpression_0uixcg8">
          <text>false</text>
        </outputEntry>
      </rule>
    </decisionTable>
  </decision>
  <dmndi:DMNDI>
    <dmndi:DMNDiagram>
      <dmndi:DMNShape dmnElementRef="inkomenstoeslagbeleid">
        <dc:Bounds height="80" width="180" x="160" y="100" />
      </dmndi:DMNShape>
    </dmndi:DMNDiagram>
  </dmndi:DMNDI>
</definitions>`
  };
  const decisionTable = await strapi.query('decision-table').create(decisionTableData);

  const stepData = {
    title: 'Inkomenstoeslag regels',
    variables: [
      { variable: inkomenVariable.id },
      { variable: geboortedatumVariable.id },
    ],
    connections: [{ connection: connection.id }],
    decisionTables: [{ decisionTable: decisionTable.id }],
    conditions: [{ condition: condition.id }]
  };
  const step = await strapi.query('step').create(stepData);

  const processData = {
    title: 'Individuele Inkomenstoeslag',
    description: 'Als u moeite heeft om aan het einde van de maand rond te komen, dan kan de gemeente u misschien helpen.',
    steps: [{ step: step.id }],
  };
  return strapi.query('process').create(processData);
}

async function createIndividueleStudietoeslag() {
  const werkSituatieVariable = await strapi.query('variable').findOne({title: 'Zoekhulp Werksituatie'});

  const afwijzingZoekhulpWerksituatieConditionData = {
    title: 'Afwijzing zoekhulp werksituatie',
    expression: '${input_string___zoekhulp_werksituatie != "student"}',
    outcome: 'Negative'
  };
  const afwijzingZoekhulpWerksituatieCondition = await strapi.query('condition').create(afwijzingZoekhulpWerksituatieConditionData);

  const zoekhulpWerksituatieStepData = {
    title: 'Zoekhulp werksituatie',
    variables: [{ variable: werkSituatieVariable.id }],
    conditions: [{ condition: afwijzingZoekhulpWerksituatieCondition.id }],
  };
  const zoekhulpWerksituatieStep = await strapi.query('step').create(zoekhulpWerksituatieStepData);

  const processData = {
    title: 'Individuele Studietoeslag',
    description: 'Een studietoeslag van van de gemeente die ervoor zorgt dat je niet van je studie afgeleid wordt door geldzorgen.',
    steps: [{ step: zoekhulpWerksituatieStep.id }],
  };
  return strapi.query('process').create(processData);
}

async function createTegemoetkomingKostenKinderopvangProcess() {
  const woonSituatieVariable = await strapi.query('variable').findOne({title: 'Zoekhulp Woonsituatie'});

  const afwijzingZoekhulpWoonSituatieConditionData = {
    title: 'Afwijzing zoekhulp woonsituatie',
    expression: '${input_string___zoekhulp_woonsituatie == "alleenstaand" || input_string___zoekhulp_woonsituatie == "samenwonend"}',
    outcome: 'Negative'
  };
  const afwijzingZoekhulpWoonSituatieCondition = await strapi.query('condition').create(afwijzingZoekhulpWoonSituatieConditionData);

  const zoekhulpWoonsituatieStepData = {
    title: 'Zoekhulp woonsituatie',
    variables: [{ variable: woonSituatieVariable.id }],
    conditions: [{ condition: afwijzingZoekhulpWoonSituatieCondition.id }],
  };
  const zoekhulpWoonsituatieStep = await strapi.query('step').create(zoekhulpWoonsituatieStepData);

  const processData = {
    title: 'Tegemoetkoming Kosten Kinderopvang',
    description: 'Extra hulp van de gemeente bij de betaling van kinderopvang, zodat u met een gerust hart aan uw carriere kunt werken.',
    steps: [{ step: zoekhulpWoonsituatieStep.id }],
  };
  return strapi.query('process').create(processData);
}

async function createStudentreistoeslagProcess() {
  const schoolGemeenteVariableData = {
    title: 'Schoolgemeente',
    description: 'Schoolgemeente',
    type: 'string',
    source: 'Input',
  };
  const schoolGemeenteVariable = await strapi.query('variable').create(schoolGemeenteVariableData);

  const SchoolgemeenteGroningenConditionData = {
    title: 'Schoolgemeente Groningen',
    expression: '${input_string___schoolgemeente != "Groningen"}',
    outcome: 'Negative'
  };
  const schoolgemeenteGroningenCondition = await strapi.query('condition').create(SchoolgemeenteGroningenConditionData);

  const schoolGemeenteCheckStepData = {
    title: 'Schoolgemeente check',
    variables: [{ variable: schoolGemeenteVariable.id }],
    conditions: [{ condition: schoolgemeenteGroningenCondition.id }],
  };
  const schoolGemeenteCheckStep = await strapi.query('step').create(schoolGemeenteCheckStepData);

  const studentLeeftijdgrensCheckStep = await strapi.query('step').findOne({title: 'Student leeftijdgrens check'});

  const processData = {
    title: 'Studentreistoeslag',
    description: 'De OV-jaarkaart voor studenten dekt niet alle soorten reiskosten. De gemeente kan helpen om verhuisvervoer of een fiets te regelen.',
    steps: [
      { step: schoolGemeenteCheckStep.id },
      { step: studentLeeftijdgrensCheckStep.id },
    ],
  };
  return strapi.query('process').create(processData);
}
