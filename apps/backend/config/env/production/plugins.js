module.exports = ({ env }) => ({
  upload: {
    provider: 'google-cloud-storage',
    providerOptions: {
      serviceAccount: env.json('GKE_SERVICE_ACCOUNT'),
      bucketName: env('GCS_BUCKET_NAME'),
    },
  },
});
