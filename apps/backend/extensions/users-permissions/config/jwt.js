const uuid = require('uuid');

module.exports = {
  jwtSecret: process.env.JWT_SECRET || uuid()
};
