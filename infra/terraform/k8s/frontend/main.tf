variable "gke_namespace" {
  type = string
}

variable "docker_image_name" {
  type = string
}

variable "server_port" {
  type = number
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

data "kubernetes_namespace" "namespace" {
  metadata {
    name = var.gke_namespace
  }
}

resource "kubernetes_deployment" "frontend_deployment" {
  wait_for_rollout = false
  metadata {
    name      = "frontend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector {
      match_labels = {
        app = "frontend"
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge = "25%"
        max_unavailable = "25%"
      }
    }
    template {
      metadata {
        labels = {
          app = "frontend"
        }
      }
      spec {
        dns_policy = "ClusterFirst"
        restart_policy = "Always"
        container {
          name = "frontend"
          image = var.docker_image_name
          port {
            container_port = var.server_port
            name = "http-frontend"
            protocol = "TCP"
          }
        }
        container {
          name = "redis"
          image = "redis:latest"
          port {
            container_port = 6379
            name = "http-redis"
            protocol = "TCP"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "frontend_service" {
  metadata {
    name      = "frontend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.frontend_deployment.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      name = "http-frontend"
      port = 80
      protocol = "TCP"
      target_port = var.server_port
    }
  }
}

resource "kubernetes_service" "redis_service" {
  metadata {
    name      = "redis"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.frontend_deployment.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      name = "http-redis"
      port = 6379
      protocol = "TCP"
      target_port = 6379
    }
  }
}
