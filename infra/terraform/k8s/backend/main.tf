provider "kubernetes" {
  config_path = "~/.kube/config"
}

data "kubernetes_namespace" "namespace" {
  metadata {
    name = var.gke_namespace
  }
}

resource "kubernetes_deployment" "deployment" {
  wait_for_rollout = false
  metadata {
    name      = "backend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector {
      match_labels = {
        app = "backend"
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge = "25%"
        max_unavailable = "25%"
      }
    }
    template {
      metadata {
        labels = {
          app = "backend"
        }
      }
      spec {
        dns_policy = "ClusterFirst"
        restart_policy = "Always"
        container {
          image = var.docker_image_name
          name  = "backend"
          port {
            container_port = var.container_port
            name = "http"
            protocol = "TCP"
          }
          env {
            name = "DB_USER"
            value_from {
              secret_key_ref {
                key = "username"
                name = "database-credentials"
              }
            }
          }
          env {
            name = "DB_PASS"
            value_from {
              secret_key_ref {
                key = "password"
                name = "database-credentials"
              }
            }
          }
          env {
            name = "DB_NAME"
            value_from {
              secret_key_ref {
                key = "database"
                name = "database-credentials"
              }
            }
          }
        }
        container {
          name = "cloud-sql-proxy"
          image= "gcr.io/cloudsql-docker/gce-proxy:1.17"
          command = [
            "/cloud_sql_proxy",
            "-instances=${data.google_sql_database_instance.postgresql.connection_name}=tcp:5432",
            "-credential_file=/secrets/service_account.json"
          ]
          security_context {
            run_as_non_root = true
          }
          volume_mount {
            mount_path = "/secrets/"
            name = "service-account"
            read_only = true
          }
        }
        volume {
          name = "service-account"
          secret {
            secret_name = "service-account"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "service" {
  metadata {
    name      = "backend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.deployment.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      name = "http"
      port = var.port
      protocol = "TCP"
      target_port = var.target_port
    }
  }
}

resource "kubernetes_secret" "database-credentials" {
  metadata {
    name = "database-credentials"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }

  data = {
    username = var.db_username
    password = var.db_password
    database = var.db_name
  }
}

resource "kubernetes_secret" "service-account" {
  metadata {
    name = "service-account"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }

  data = {
    "service_account.json" = file(var.credentials)
  }
}

provider "google" {
  credentials = file(var.credentials)

  project = var.project
  region = var.region
  zone = var.zone
}

data "google_sql_database_instance" "postgresql" {
  name = var.db_instance_name
}
