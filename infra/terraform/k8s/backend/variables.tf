variable "gke_namespace" {
  type = string
}

variable "docker_image_name" {
  type = string
}

variable "container_port" {
  type = number
  default = 80
}

variable "port" {
  type = number
  default = 80
}

variable "target_port" {
  type = number
  default = 80
}

variable "credentials" {
  type = string
}

variable "db_instance_name" {
  type = string
}

variable "db_name" {
  type = string
}

variable "db_username" {
  type = string
}

variable "db_password" {
  type = string
  default = "root"
}

variable "zone" {
  type = string
}

variable "region" {
  type = string
}

variable "project" {
  type = string
}

