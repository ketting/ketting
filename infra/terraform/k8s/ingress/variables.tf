variable "gke_namespace" {
  type = string
}

variable "backend_host" {
  type = string
}

variable "frontend_host" {
  type = string
}

variable "ssl_cert_name" {
  type = string
}

variable "credentials" {
  type = string
}

variable "zone" {
  type = string
}

variable "region" {
  type = string
}

variable "project" {
  type = string
}
