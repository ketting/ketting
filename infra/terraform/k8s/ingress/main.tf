provider "google" {
  credentials = file(var.credentials)

  project = var.project
  region = var.region
  zone = var.zone
}

resource "google_compute_managed_ssl_certificate" "default" {
  name = var.ssl_cert_name

  managed {
    domains = [var.backend_host, var.frontend_host]
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

data "kubernetes_namespace" "namespace" {
  metadata {
    name = var.gke_namespace
  }
}

data "kubernetes_service" "camunda_service" {
  metadata {
    name = "camunda"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
}

data "kubernetes_service" "backend_service" {
  metadata {
    name = "backend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
}

data "kubernetes_service" "frontend_service" {
  metadata {
    name = "frontend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
}

resource "kubernetes_ingress" "backend_ingress" {
  wait_for_load_balancer = true
  metadata {
    name = "backend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.global-static-ip-name" = "backend-external-ip"
      "ingress.gcp.kubernetes.io/pre-shared-cert" = google_compute_managed_ssl_certificate.default.name
      "kubernetes.io/ingress.class" = "gce"
    }
  }
  spec {
    rule {
      host = var.backend_host
      http {
        path {
          path = "/camunda"
          backend {
            service_name = data.kubernetes_service.camunda_service.metadata.0.name
            service_port = data.kubernetes_service.camunda_service.spec.0.port.0.port
          }
        }
        path {
          path = "/camunda/*"
          backend {
            service_name = data.kubernetes_service.camunda_service.metadata.0.name
            service_port = data.kubernetes_service.camunda_service.spec.0.port.0.port
          }
        }
        path {
          path = "/*"
          backend {
            service_name = data.kubernetes_service.backend_service.metadata.0.name
            service_port = data.kubernetes_service.backend_service.spec.0.port.0.port
          }
        }
      }
    }
  }
}

resource "kubernetes_ingress" "frontend_ingress" {
  wait_for_load_balancer = true
  metadata {
    name = "frontend"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.global-static-ip-name" = "frontend-external-ip"
      "ingress.gcp.kubernetes.io/pre-shared-cert" = google_compute_managed_ssl_certificate.default.name
      "kubernetes.io/ingress.class" = "gce"
    }
  }
  spec {
    rule {
      host = var.frontend_host
      http {
        path {
          path = "/*"
          backend {
            service_name = data.kubernetes_service.frontend_service.metadata.0.name
            service_port = data.kubernetes_service.frontend_service.spec.0.port.0.port
          }
        }
      }
    }
  }
}
