variable "gke_namespace" {
  type = string
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    name = var.gke_namespace
  }
}

resource "kubernetes_deployment" "camunda_deployment" {
  wait_for_rollout = false
  metadata {
    name      = "camunda"
    namespace = kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector {
      match_labels = {
        app = "camunda"
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge = "25%"
        max_unavailable = "25%"
      }
    }
    template {
      metadata {
        labels = {
          app = "camunda"
        }
      }
      spec {
        dns_policy = "ClusterFirst"
        restart_policy = "Always"
        container {
          name = "camunda-bpm"
          image = "camunda/camunda-bpm-platform:latest"
          port {
            container_port = 8080
            name = "http-camunda"
            protocol = "TCP"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "camunda_service" {
  metadata {
    name      = "camunda"
    namespace = kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.camunda_deployment.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      name = "http-camunda"
      port = 8080
      protocol = "TCP"
      target_port = 8080
    }
  }
}
