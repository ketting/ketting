variable "gke_namespace" {
  type = string
}

variable "docker_image_name" {
  type = string
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

data "kubernetes_namespace" "namespace" {
  metadata {
    name = var.gke_namespace
  }
}

resource "kubernetes_deployment" "worker_deployment" {
  wait_for_rollout = false
  metadata {
    name      = "worker"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector {
      match_labels = {
        app = "worker"
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge = "25%"
        max_unavailable = "25%"
      }
    }
    template {
      metadata {
        labels = {
          app = "worker"
        }
      }
      spec {
        dns_policy = "ClusterFirst"
        restart_policy = "Always"
        container {
          name = "worker"
          image = var.docker_image_name
          port {
            container_port = 8000
            name = "http-worker"
            protocol = "TCP"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "worker_service" {
  metadata {
    name      = "worker"
    namespace = data.kubernetes_namespace.namespace.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.worker_deployment.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      name = "http-worker"
      port = 8000
      protocol = "TCP"
      target_port = 8000
    }
  }
}
