provider "google" {
  credentials = file(var.credentials)

  project = var.project
  region = var.region
  zone = var.zone
}

resource "google_container_cluster" "primary" {
  name     = var.cluster_name
  location = var.region

  enable_autopilot = true
}

resource "google_sql_database_instance" "postgresql" {
  name = var.db_instance_name
  database_version = "POSTGRES_13"
  region = var.region
  project = var.project

  settings {
    tier = var.db_tier
    backup_configuration {
      enabled = true
    }
  }
}

resource "google_sql_user" "users" {
  name     = var.db_username
  instance = google_sql_database_instance.postgresql.name
  password = var.db_password
}

resource "google_sql_database" "database" {
  name     = var.db_name
  instance = google_sql_database_instance.postgresql.name
}

resource "google_compute_global_address" "backend" {
  name = "backend-external-ip"
}

resource "google_compute_global_address" "frontend" {
  name = "frontend-external-ip"
}

resource "google_storage_bucket" "static-site" {
  name          = var.bucket_name
  location      = var.region
  force_destroy = true

  cors {
    origin          = ["*"]
    method          = ["GET", "HEAD", "PUT", "POST", "DELETE"]
    response_header = ["*"]
  }
}