variable "project" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "zone" {
  type = string
}

variable "region" {
  type = string
}

variable "credentials" {
  type = string
}

variable "db_tier" {
  type = string
  default = "db-f1-micro"
}

variable "db_instance_name" {
  type = string
}

variable "db_password" {
  type = string
  default = "root"
}

variable "db_username" {
  type = string
}

variable "db_name" {
  type = string
}

variable "bucket_name" {
  type = string
}
