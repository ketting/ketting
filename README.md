# Ketting

Model and execute complex business processes as simple unidimensional chains.

- [Project website](https://www.ketting.io) - Showcases and general information
- [Demo application](https://demo.ketting.io) - Try out all features (resets every 24 hours)
- [Documentation](https://docs.ketting.io) - Features, architecture, technical details and examples
- [OpenAPI](https://demo.ketting.io/api) - Living documentation in OAS 3.0 format
- [Zero configuration](#Developing) - Up and running with a single command
- [Test automation](#Testing) - test-driven development and continuous integration
- [Cloud native](#Deployment) - Runs on GKE, EKS and AKS
- [Open source](#License) - MIT License

**todo:** embed video here

## Quick Start

### Prerequisites

- [skaffold](https://skaffold.dev/docs/quickstart/)
- [git](https://git-scm.com/)
- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [docker](https://www.docker.com/get-started/)
- [ngrok](https://ngrok.com/)

```sh
git clone git@gitlab.com:ketting/ketting.git
cd ketting
~/.ngrok http 3001
```

### Run the application

#### Apple M1 machine
Additional steps need to be done before applications can be run with skaffold or docker-compose on docker desktop Apple M1 machine
1. Add `export QEMU_CPU=max` before `yarn develop` in `run.sh` file in apps/backend directory
2. Pull strapi image with platform linux/amd64 in Dockerfile
```sh
FROM --platform=linux/amd64 strapi/base
```

#### with skaffold
1. Update webhook URL in skaffold yaml file to the link provided by ngrok and add `/api/wallet/callback` as path

```sh
minikube start
skaffold dev
```

#### with docker-compose
1. Create .env file in root directory and fill it with the following example:
   CAMUNDA_URL=http://camunda:8080/engine-rest
   JWT_SECRET=changeme
   SERVER_PORT=3001
   API_BASE_URL=http://strapi:1337
   API_IDENTIFIER=groningen@ketting.io
   API_PASSWORD=Changem3
   NODE_ENV=development
   POSTGRES_USER=example-user
   POSTGRES_PASSWORD=example-password
   POSTGRES_DB=strapi
   DATABASE_HOST=postgres
   WALLET_BASE_URL=https://wallet-api.ovrhd.nl
   WEBHOOK_URL=the-link-provided-by-ngrok/api/wallet/callback
   SERVICE_LEDGER_BASE_URL=https://service.ledgr.nl
   REDIS_HOST=redis
2. Change the value in the .env file 

```sh
docker-compose up
```

## Developing

**todo**

## Testing

**todo**

## Deployment

All files required to install the software manually/on-premise are included in this repository. The software can also be deployed to and run in the cloud.

### Manual installation

#### Prerequisites

- [terraform](https://www.terraform.io/intro/index.html/)
- [google cloud sdk](https://cloud.google.com/sdk/docs/quickstart/)

### Google Kubernetes Engine

#### with terraform from local dev

- Connect to google kubernetes engine cluster
- Create a service account in google cloud and save the service account key json file
- Put the required variables in a file with tfvars extension for each service, e.g `terraform.tfvars`
- Run the following commands to trigger the deployment in the respective directory that is needed to be applied
```sh
terraform init
terraform validate
terraform plan
terraform apply
```

#### with terraform via gitlab ci

- Create a service account in google cloud and save the service account key json file as a CI variable with the name GKE_SERVICE_ACCOUNT 
- Add the required variables to the CI variable and save it with the name TERRAFORM_VARIABLES
- Add all the other variables listed in the gitlab-ci file to the CI variables
- Commit and push to the repository

### Azure Kubernetes Service

**todo**

### Amazon Elastic Kubernetes Service

**todo**

## Acknowledgements

We are grateful to Marc Minnee for the original idea behind this project

- @marc4gov - Marc Minnee


## License

Ketting is open source software [licensed as MIT](https://gitlab.com/ketting/ketting/-/blob/master/LICENSE).
